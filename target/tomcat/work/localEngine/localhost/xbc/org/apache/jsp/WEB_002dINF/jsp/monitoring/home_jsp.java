package org.apache.jsp.WEB_002dINF.jsp.monitoring;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/WEB-INF/jsp/monitoring/addMonitoring.jsp");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("\t<div class=\"col-sm-12\">\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"box box-primary\">\r\n");
      out.write("\t\t\t<div class=\"box-header with-border\">\r\n");
      out.write("\t\t\t\t<h2>IDLE MONITORING</h2>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t\t<div class=\"box-body\">\r\n");
      out.write("\t\t\t\t<div>\r\n");
      out.write("\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\"\r\n");
      out.write("\t\t\t\t\t\tdata-target=\"#addMonitoring\">+</button>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<div class=\"modal fade\" id=\"addMonitoring\" role=\"dialog\"\r\n");
      out.write("\tdata-parsley-validate>\r\n");
      out.write("\t<div class=\"modal-dialog\">\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- modal content  -->\r\n");
      out.write("\t\t<div class=\"modal-content\">\r\n");
      out.write("\t\t\t<div class=\"modal-header\">\r\n");
      out.write("\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n");
      out.write("\t\t\t\t<h3 class=\"modal-title\">INPUT IDLE</h3>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<div class=\"modal-body\">\r\n");
      out.write("\t\t\t\t<form class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" id=\"idleName\" placeholder=\"Name\" class=\"form-control\">\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"date\" id=\"idleDate\" placeholder=\"Idle Date\" class=\"form-control\">\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" id=\"idleLastProjectAt\" placeholder=\"Last Project At\" class=\"form-control\">\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" id=\"idleReason\" placeholder=\"Idle Reason\" class=\"form-control\">\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t</form>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<div>\r\n");
      out.write("\t\t\t\t<div class=\"modal-footer\">\r\n");
      out.write("\t\t\t\t\t<button type=\"button\" id=\"btnSaveIdle\" class=\"btn btn-primary\">Save</button>\r\n");
      out.write("\t\t\t\t\t<button type=\"button\" id=\"btnCancelIdle\" class=\"btn btn-primary\" data-dismiss=\"modal\">Cancel</button>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("</html>");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("\r\n");
      out.write("$(function(){\r\n");
      out.write("\t$(\"#btnSaveIdle\").click(function(){\r\n");
      out.write("\t\tsaveIdle();\r\n");
      out.write("\t});\r\n");
      out.write("})\r\n");
      out.write("\r\n");
      out.write(" function saveIdle() {\r\n");
      out.write("\t $.ajax({\r\n");
      out.write("\t\t type \t: \"POST\",\r\n");
      out.write("\t\t url\t: \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/monitoring/save\",\r\n");
      out.write("\t\t data\t: {\r\n");
      out.write("\t\t\t idleName\t: $(\"#idleName\").val(),\r\n");
      out.write("\t\t\t idleDate\t: $(\"#idleDate\").val(),\r\n");
      out.write("\t\t\t idleLastProjectAt : $(\"#idleLastProjectAt\").val(),\r\n");
      out.write("\t\t\t idleReason\t: $(\"#idleReason\"),\r\n");
      out.write("\t\t\t success\t: function(data){\r\n");
      out.write("\t\t\t\t console.log(data);\r\n");
      out.write("\t\t\t }, error : function(){\r\n");
      out.write("\t\t\t\t alert(\"insert data faild\");\r\n");
      out.write("\t\t\t }\r\n");
      out.write("\t\t }\r\n");
      out.write("\t });\r\n");
      out.write(" }\r\n");
      out.write(" \r\n");
      out.write("</script>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
