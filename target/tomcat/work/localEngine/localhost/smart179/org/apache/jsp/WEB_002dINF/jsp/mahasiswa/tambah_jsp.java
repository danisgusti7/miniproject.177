package org.apache.jsp.WEB_002dINF.jsp.mahasiswa;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class tambah_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<form action=\"#\" method=\"get\" id=\"form-mahasiswa-tambah\" class=\"form-horizontal\">\r\n");
      out.write("\t<div class=\"form-group\">\r\n");
      out.write("\t\t<label class=\"control-label col-md-3\" for=\"kode\">Kode Mahasiswa</label>\r\n");
      out.write("\t\t<div class=\"col-md-8\">\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"kode\" name=\"kodeMahasiswa\" class=\"form-control\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"form-group\">\r\n");
      out.write("\t\t<label class=\"control-label col-md-3\" for=\"nama\">Nama Mahasiswa</label>\r\n");
      out.write("\t\t<div class=\"col-md-8\">\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"nama\" name=\"namaMahasiswa\" class=\"form-control\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"form-group\">\r\n");
      out.write("\t\t<label class=\"control-label col-md-3\" for=\"jk\">Gender</label>\r\n");
      out.write("\t\t<div class=\"col-md-8\">\r\n");
      out.write("\t\t\t<input type=\"radio\" value=\"pria\" name=\"jkMahasiswa\">Pria\r\n");
      out.write("\t\t\t<input type=\"radio\" value=\"wanita\" name=\"jkMahasiswa\">Wanita\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t<label class=\"control-label col-md-3\" for=\"alamat\">Alamat Mahasiswa</label>\r\n");
      out.write("\t\t<div class=\"col-md-8\">\r\n");
      out.write("\t\t\t<textarea rows=\"5\" cols=\"10\" name=\"alamatMahasiswa\" id=\"alamat\" class=\"form-control\"></textarea>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"modal-footer\">\r\n");
      out.write("\t\t<button type=\"submit\" class=\"btn btn-primary btn-sm\">Simpan</button>\r\n");
      out.write("\t</div>\r\n");
      out.write("</form>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
