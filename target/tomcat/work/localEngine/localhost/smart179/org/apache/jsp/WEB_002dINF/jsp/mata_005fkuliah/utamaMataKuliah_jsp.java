package org.apache.jsp.WEB_002dINF.jsp.mata_005fkuliah;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class utamaMataKuliah_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<br/>\r\n");
      out.write("<br/>\r\n");
      out.write("<br/>\r\n");
      out.write("<div style=\"background-color: white; height: 800px;\">\r\n");
      out.write("\t<h2>HALAMAN UTAMA MATA KULIAH</h2>\r\n");
      out.write("\t<button type=\"button\" id=\"button-tambah\">Tambah</button>\r\n");
      out.write("\t<div>\r\n");
      out.write("\t\t<table class=\"table\" id=\"table-matakuliah\">\r\n");
      out.write("\t\t\t<tr>\r\n");
      out.write("\t\t\t\t<td>No</td>\r\n");
      out.write("\t\t\t\t<td>Kode Mata Kuliah</td>\r\n");
      out.write("\t\t\t\t<td>Mata Kuliah</td>\r\n");
      out.write("\t\t\t\t<td>SKS</td>\r\n");
      out.write("\t\t\t\t<td>Biaya</td>\r\n");
      out.write("\t\t\t\t<td>Action</td>\r\n");
      out.write("\t\t\t</tr>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<tbody id=\"list-data-matakuliah\">\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t</tbody>\r\n");
      out.write("\t\t</table>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<div class=\"modal fade\" id=\"modal-input\">\r\n");
      out.write("\t\t<div class=\"modal-dialog\">\r\n");
      out.write("\t\t\t<div class=\"modal-content\">\r\n");
      out.write("\t\t\t\t<div class=\"modal-body\">\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div> <!-- //POPUP -->\r\n");
      out.write("\t\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<script>\r\n");
      out.write("\r\n");
      out.write("\tlistDataMataKuliah();\r\n");
      out.write("\tfunction listDataMataKuliah(){\r\n");
      out.write("\t\t$.ajax({\r\n");
      out.write("\t\t\turl:\"mata_kuliah/listMataKuliah.html\",\r\n");
      out.write("\t\t\ttype: \"get\",\r\n");
      out.write("\t\t\tdataType: \"html\",\r\n");
      out.write("\t\t\tsuccess: function(result) {\r\n");
      out.write("\t\t\t\t$(\"#list-data-matakuliah\").html(result);\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t});\r\n");
      out.write("\t\t\r\n");
      out.write("\t}\r\n");
      out.write("\t\r\n");
      out.write("\t$(document).ready(function(){\r\n");
      out.write("\t\t$(\"#button-tambah\").on(\"click\", function(){\r\n");
      out.write("\t\t\t$.ajax({\r\n");
      out.write("\t\t\t\turl:\"mata_kuliah/tambahMataKuliah.html\",\r\n");
      out.write("\t\t\t\ttype: \"get\",\r\n");
      out.write("\t\t\t\tdataType: \"html\",\r\n");
      out.write("\t\t\t\tsuccess: function(result) {\r\n");
      out.write("\t\t\t\t\t$(\"#modal-input\").find(\".modal-body\").html(result);\t\r\n");
      out.write("\t\t\t\t\t$(\"#modal-input\").modal(\"show\");\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t});\r\n");
      out.write("\t\t});\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t$(\"#modal-input\").on(\"submit\", \"#form-matakuliah-tambah\", function(){\r\n");
      out.write("\t\t\t$.ajax({\r\n");
      out.write("\t\t\t\turl:\"mata_kuliah/create.json\", //menambah data ke back-end\r\n");
      out.write("\t\t\t\ttype: \"get\",\r\n");
      out.write("\t\t\t\tdataType: \"json\",\r\n");
      out.write("\t\t\t\tdata:$(this).serialize(),\r\n");
      out.write("\t\t\t\tsuccess: function(result) {\r\n");
      out.write("\t\t\t\t\t$(\"#modal-input\").modal(\"hide\");\r\n");
      out.write("\t\t\t\t\talert(\"Mata Kuliah tersimpan\");\r\n");
      out.write("\t\t\t\t\tlistDataMataKuliah();\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t});\r\n");
      out.write("\t\t\treturn false;\r\n");
      out.write("\t\t});\r\n");
      out.write("\t\t\r\n");
      out.write("\t});\r\n");
      out.write("</script>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
