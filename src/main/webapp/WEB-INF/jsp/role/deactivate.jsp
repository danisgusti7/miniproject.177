<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-deactivate-role" class="form-horizontal">
<input type="hidden" id="id" name="id" value="${role.id}"/>
	<input type="hidden" id="code" name="code" value="${role.code}"/>
	<div class="form-group">
		<label class="control-label col-md-3" for="code">Code</label>
		<div class="col-md-8">
			<input type="text" value="${role.code}" class="form-control" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="name">Name</label>
		<div class="col-md-8">
			<input type="text" id="name" name="name" value="${role.name}" class="form-control" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="description">Description</label>
		<div class="col-md-8">
			<textarea row="5" cols="10" id="description" name="description" class="form-control" readonly>${role.description}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="status">Status</label>
		<div class="col-md-8">
			<c:if test="${role.active == 0}">
				<td class="form-control">Idle</td>
			</c:if>
			<c:if test="${role.active == 1}">
				<td class="form-control">Active</td>
			</c:if>
		</div>
	</div>
	
	<div class="modal-footer">
		<button type="submit" id="button-submit-deactivate" class="btn btn-primary btn-sm">Deactivate</button>
		<button type="button" id="button-cancel-deactivate" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>