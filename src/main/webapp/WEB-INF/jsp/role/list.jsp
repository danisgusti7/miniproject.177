<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:forEach items="${roleList}" var="role" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${role.code}</td>
		<td>${role.name}</td>
		<c:if test="${role.active == 1}">
			<td>Active</td>
		</c:if>
		<c:if test="${role.active == 0}">
			<td>Idle</td>
		</c:if>
		<td>
			<button type="button" id="button-edit" value="${role.id}" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>
			<button type="button" id="button-deactivate" value="${role.id}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>

		</td>
	</tr>

</c:forEach>