<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Role</h3>
		<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 250px;">
				<input type="text" id="nameSearch" name="nameSearch"
					placeholder="Search by Name" class="form-control pull-right" />

				<div class="input-group-btn">
					<button type="button" id="button-search" class="btn btn-default">
						<i class="fa fa-search"></i>
					</button>
					<button type="button" id="button-add-role"
						class="btn btn-primary btn-sm">
						<i class="fa fa-plus"></i> Add
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="table-role">
			<thead>
				<tr>
					<td>No</td>
					<td>Code</td>
					<td>Name</td>
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>

			<tbody id="list-role">

			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<!-- //POPUP -->


<script>
	listRole();
	function listRole() {
		$.ajax({
			url : "role/list.html",
			type : "POST",
			dataType : "html",
			success : function(result) {
				$("#list-role").html(result);

			}
		});
	}

	$(document).ready(
			function() {
				$("#button-add-role").on(
						"click",
						function() {
							$.ajax({
								url : "role/add.html",
								type : "POST",
								dataType : "html",
								success : function(result) {
									$("#modal-input").find(".modal-title")
											.html("Add Role");
									$("#modal-input").find(".modal-body").html(
											result);
									$("#modal-input").modal("show");
								}
							});
						});

				$("#modal-input").on("submit", "#form-add-role", function() {
					var name = $("#modal-input").find("#name").val();
					var description = $("#modal-input").find("#description").val();
					// \d = digits [0-9]
					// \w = [a-zA-Z0-9]
					// \S matches anything but a whitespace
					// \s matches whitespace (spaces, tabs and new lines)
					// + means 1 or more * means 0 or more
					var validName = /^[a-z]+$/i;
					//cannot start with space, accept space midway and cannot contain numbers + case insensitive
					var fieldNum = /^[^-\s][a-z_\s-]+$/i;
					if(name == ""){
						alert("Name cannot be Empty");
					}else if(!(name.match(validName))){
						alert("Name cannot start with space and cannot contain numbers")
					}else if(description == ""){
						alert("Description cannot be empty")
					}else{
						$.ajax({
							url : "role/create.json", //menambah data ke back-end
							type : "POST",
							dataType : "json",
							data : $(this).serialize(),
							success : function(result) {
								$("#modal-input").modal("hide");
								alert("Role Saved");
								listRole();
							}
						});
					}
					//return false tutjuannya untuk tidak meng-redirect halaman
					return false;
				});

				$("#button-search").on("click", function() {
					var nameSearch = $("#nameSearch").val();
					$.ajax({
						url : "role/search/name.html",
						type : "POST",
						dataType : "html",
						data : {
							nameSearch : nameSearch
						},
						success : function(result) {
							$("#list-role").html(result);
						}
					});
					return false;
				});

				$("#list-role").on(
						"click",
						"#button-edit",
						function() {
							var idEdit = $(this).val();
							$.ajax({
								url : "role/edit.html",
								type : "POST",
								dataType : "html",
								data : {
									id : idEdit
								},
								success : function(result) {
									$("#modal-input").find(".modal-title")
											.html("Edit Role");
									$("#modal-input").find(".modal-body").html(
											result);
									$("#modal-input").modal("show");
								}
							});
						});

				$("#modal-input").on("submit", "#form-edit-role", function() {
					var name = $("#modal-input").find("#name").val();
					var description = $("#modal-input").find("#description").val();
					var validName = /^[a-z]+$/i;
					if(name == ""){
						alert("Name cannot be Empty");
					}else if(!(name.match(validName))){
						alert("Name cannot start with space and cannot contain numbers")
					}else if(description == ""){
						alert("Description cannot be empty")
					}else{
						$.ajax({
							url : "role/edit/save.json",
							type : "POST",
							dataType : "json",
							data : $(this).serialize(),
							success : function(result) {
								$("#modal-input").modal("hide");
								alert("Changes have been saved");
								listRole();
							}
						});
					}		
					return false;
				});

				$("#list-role").on(
						"click",
						"#button-deactivate",
						function() {
							var idDeactivate = $(this).val();
							$.ajax({
								url : "role/deactivate.html",
								type : "POST",
								dataType : "html",
								data : {
									id : idDeactivate
								},
								success : function(result) {
									$("#modal-input").find(".modal-title")
											.html("Deactivate This Role?");
									$("#modal-input").find(".modal-body").html(
											result);
									$("#modal-input").modal("show");
								}
							});
						});

				$("#modal-input").on("submit", "#form-deactivate-role",
						function() {
							$.ajax({
								url : "role/deactivate/status.json",
								type : "POST",
								dataType : "json",
								data : $(this).serialize(),
								success : function(result) {
									$("#modal-input").modal("hide");
									alert("Status Has Changed");
									listRole();
								}

							});
							return false;
						});

			});


</script>