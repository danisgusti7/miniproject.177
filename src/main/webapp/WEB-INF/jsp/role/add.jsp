<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.Random"%>
<%
Random rng = new Random();
String characters ="0123456789";
char[] pattern = new char[6];
pattern[0]='R';
pattern[1]='O';
for(int i = 2; i < pattern.length ; i++) {
	pattern[i] = characters.charAt(rng.nextInt(characters.length()));
}
String randomCode = new String(pattern);
%>


<form action="#" method="POST" id="form-add-role" class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-3" for="code">Code</label>
		<div class="col-md-8">
			<input type="text" id="code" name="code" value="${newCode}" placeholder ="${newCode}" class="form-control" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="name">Name</label>
		<div class="col-md-8">
			<input type="text" id="name" name="name" placeholder="Name" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="description">Description</label>
		<div class="col-md-8">
			<textarea rows="5" cols="10" id="description" name="description" placeholder="Description" class="form-control"></textarea>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-add" class="btn btn-primary btn-sm">Save</button>
		<button type="button" id="button-submit-cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>