<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-deactivate-menu" class="form-horizontal">
<input type="hidden" id="id" name="id" value="${menu.id}"/>
<input type="hidden" id="menuParent.id" name="menuParent.id" value="${menu.menuParent.id}"/>
	<div class="form-group">
		<label class="control-label col-md-3" for="code">Code</label>
		<div class="col-md-8">
			<input type="text" id="code" name="code" value="${menu.code}" class="form-control" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="title">Title</label>
		<div class="col-md-8">
			<input type="text" id="title" name="title" value="${menu.title}" class="form-control" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="description">Description</label>
		<div class="col-md-8">
			<textarea rows="5" cols="10" id="description" name="description" class="form-control" readonly>${menu.description}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="imageUrl">Image URL</label>
		<div class="col-md-8">
			<input type="text" id="imageUrl" name="imageUrl" value="${menu.imageUrl}" class="form-control" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="menuOrder">Menu Order</label>
		<div class="col-md-8">
			<input type="text" id="menuOrder" name="menuOrder" value="${menu.menuOrder}" class="form-control" readonly>
		</div>
	</div>
		<div class = "form-group">
		<label class="control-label col-md-3" for="menu">Menu</label>
		<div class="col-md-8">
			<select id="menu.id" name="menu.id" class="form-control" disabled>
			<option selected value="${menu.menuParent.id}">${menu.menuParent.title}</option>
				<option value="">NO PARENT</option>
				<c:forEach items="${menus}" var ="menus">
					<option value="${menus.id}">${menus.title}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="menuUrl">Menu URL</label>
		<div class="col-md-8">
			<input type="text" id="menuUrl" name="menuUrl" value="${menu.menuUrl}" class="form-control" readonly>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-edit" class="btn btn-primary btn-sm">Deactivate</button>
		<button type="button" id="button-submit-cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>