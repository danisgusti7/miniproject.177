<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach items="${menuList}" var="menu" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${menu.code}</td>
		<td>${menu.title}</td>
		
		<c:choose>
    		<c:when test="${menu.menuParent.title == null}">
    			<td>NO PARENT</td>
    		</c:when>    
   			<c:otherwise>
   				<td>${menu.menuParent.title}</td>
   			</c:otherwise>
		</c:choose>
		
		<%-- <td>${menu.menuParent.title}</td> --%>
		<c:if test="${menu.active == 1}">
			<td>Active</td>
		</c:if>
		<c:if test="${menu.active == 0}">
			<td>Idle</td>
		</c:if>
		<td>
			<button type="button" id="button-edit" value="${menu.id}" class ="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>
			<button type="button" id="button-deactivate" value="${menu.id}" class="btn btn-danger btn-xs"><i class="fa fa-times-rectangle-o"></i></button>
		</td>
		
	</tr>

</c:forEach>