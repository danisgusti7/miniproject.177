<%@page import="java.util.Random"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
Random rng= new Random();
String characters ="0123456789";
char[] pattern = new char[5];
pattern[0]='M';
for(int i = 1; i < pattern.length ; i++){
	pattern[i] = characters.charAt(rng.nextInt(characters.length()));
}
String randomCode = new String(pattern);
%>



<form action="#" method="POST" id="form-add-menu" class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-3" for="code">Code</label>
		<div class="col-md-8">
			<input type="text" id="code" name="code" value="${newCode}" placeholder ="${newCode}" class="form-control" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="title">Title</label>
		<div class="col-md-8">
			<input type="text" id="title" name="title" placeholder="Title" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="description">Description</label>
		<div class="col-md-8">
			<textarea rows="5" cols="10" id="description" name="description" placeholder="Description" class="form-control"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="imageUrl">Image URL</label>
		<div class="col-md-8">
			<input type="text" id="imageUrl" name="imageUrl" placeholder="Image URL" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="menuOrder">Menu Order</label>
		<div class="col-md-8">
			<input type="text" id="menuOrder" name="menuOrder" placeholder="Menu Order" class="form-control">
		</div>
	</div>
		<div class = "form-group">
		<label class="control-label col-md-3" for="menu">Menu</label>
		<div class="col-md-8">
			<select id="menu.id" name="menu.id" class="form-control">
				<option value="">NO PARENT</option>
				<c:forEach items="${menuList}" var ="menu">
					<option value="${menu.id}">${menu.title}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="menuUrl">Menu URL</label>
		<div class="col-md-8">
			<input type="text" id="menuUrl" name="menuUrl" placeholder="Menu URL" class="form-control">
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-add" class="btn btn-primary btn-sm">Save</button>
		<button type="button" id="button-submit-cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>