<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Menu</h3>
		<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 250px;">
				<input type="text" id="titleSearch" name="titleSearch"
					placeholder="Search by Title" class="form-control pull-right" />
				<div class="input-group-btn">
					<button type="button" id="button-search" class="btn btn-default">
						<i class="fa fa-search"></i>
					</button>
					<button type="button" id="button-add-menu"
						class="btn btn-primary btn-sm">
						<i class="fa fa-plus"></i>Add
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="table-menu">
			<thead>
				<tr>
					<td>No</td>
					<td>Code</td>
					<td>Title</td>
					<td>Parent</td>
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>

			<tbody id="list-menu">

			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<!-- //POPUP -->


<!-- A page can't be manipulated safely until the document is "ready." 
jQuery detects this state of readiness for you. 
Code included inside $( document ).ready() will only run 
once the page Document Object Model (DOM) is 
ready for JavaScript code to execute -->

<!-- The serialize() method creates a URL encoded text string by serializing form values.

You can select one or more form elements (like input and/or text area), or the form element itself.

The serialized values can be used in the URL query string when making an AJAX request. -->

<script>
	listMenu();
	function listMenu() {
		$.ajax({
			//dalam jQuery bisa isi apa aja
			url : "menu/list.html",
			type : "POST",
			dataType : "html",
			//fucntion() _. isinya variabel baru?
			success : function(result) {
				$("#list-menu").html(result);
			}
		});
	}
	//document itu apa? 
	//.ready() itu apa?
	//.on()
	$(document).ready(
			function() {
				$("#button-add-menu").on(
						"click",
						function() {
							$.ajax({
								url : "menu/add.html",
								type : "POST",
								dataType : "html",
								success : function(result) {
									$("#modal-input").find(".modal-title")
											.html("Add Menu");
									$("#modal-input").find(".modal-body").html(
											result);
									$("#modal-input").modal("show");
								}

							});

						});

				$("#modal-input").on("submit", "#form-add-menu", function() {
					var title = $("#modal-input").find("#title").val();
					var description = $("#modal-input").find("#description").val();
					var imageUrl = $("#modal-input").find("#imageUrl").val();
					var menuOrder = $("#modal-input").find("#menuOrder").val();
					var menuUrl = $("#modal-input").find("#menuUrl").val();
					var validMenuOrder = /^[0-9]+$/;
					if(title =="" || description=="" || imageUrl=="" || menuOrder=="" || menuUrl ==""){
						alert("Fields cannnot be empty");
					}else if(!(menuOrder.match(validMenuOrder))){
						alert("Menu Order must be in numbers");
					}else{
						$.ajax({
							//cara mengirim data ke json
							url : "menu/create.json",
							type : "POST",
							dataType : "json",
							//this nunjuk siapa?
							//serialize() untuk apa?
							data : $(this).serialize(),
							success : function(result) {
								$("#modal-input").modal("hide");
								alert("Menu Saved");
								listMenu();
							}
						});
					}
					//return false apa ini??
					return false;
				});

				$("#button-search").on(
						"click",
						function() {
							var titleSearch = $("#titleSearch").val();
							$.ajax({
								url : "menu/search/title.html",
								type : "POST",
								dataType : "html",
								//maskud isi kurung kurawal itu apa?
								data : {
									titleSearch : titleSearch
								},
								success : function(result) {
									$("#list-menu").html(result);
								}
							});
							return false;
						});

				$("#list-menu").on(
						"click",
						"#button-edit",
						function() {
							var idEdit = $(this).val();
							$.ajax({
								url : "menu/edit.html",
								type : "POST",
								dataType : "html",
								data : {
									id : idEdit
								},
								success : function(result) {
									$("#modal-input").find(".modal-title")
											.html("Edit Menu");
									$("#modal-input").find(".modal-body").html(
											result);
									$("#modal-input").modal("show");
								}

							});
						});

				$("#modal-input").on("submit", "#form-edit-menu", function() {
					var title = $("#modal-input").find("#title").val();
					var description = $("#modal-input").find("#description").val();
					var imageUrl = $("#modal-input").find("#imageUrl").val();
					var menuOrder = $("#modal-input").find("#menuOrder").val();
					var menuUrl = $("#modal-input").find("#menuUrl").val();
					var validMenuOrder = /^[0-9]+$/;
					if(title =="" || description=="" || imageUrl=="" || menuOrder=="" || menuUrl ==""){
						alert("Fields cannnot be empty");
					}else if(!(menuOrder.match(validMenuOrder))){
						alert("Menu Order must be in numbers");
					}else{
						$.ajax({
							url : "menu/edit/save.json",
							type : "POST",
							dataType : "json",
							data : $(this).serialize(),
							success : function(result) {
								$("#modal-input").modal("hide");
								alert("Changes has been saved");
								listMenu();
							}
						});
					}
					
					return false;
				});

				$("#list-menu").on(
						"click",
						"#button-deactivate",
						function() {
							var idDeactivate = $(this).val();
							$.ajax({
								url : "menu/deactivate.html",
								type : "POST",
								dataType : "html",
								//id dapat dari mana?
								data : {
									id : idDeactivate
								},
								success : function(result) {
									$("#modal-input").find(".modal-title")
											.html("Deactivate This Menu?");
									$("#modal-input").find(".modal-body").html(
											result);
									$("#modal-input").modal("show");
								}
							});

						});

				$("#modal-input").on("submit", "#form-deactivate-menu",
						function() {
							$.ajax({
								url : "menu/deactivate/save.json",
								type : "POST",
								dataType : "json",
								data : $(this).serialize(),
								success : function(result) {
									$("#modal-input").modal("hide");
									alert("Status has Changed");
									listMenu();
								}
							});
							return false;
						});

			});
</script>