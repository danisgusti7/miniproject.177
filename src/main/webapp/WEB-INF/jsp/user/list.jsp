<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach items="${userList}" var="user" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${user.username}</td>
		<td>${user.roleUser.name}</td>
		<c:if test="${user.active == 1}">
			<td>Active</td>
		</c:if>
		<c:if test="${user.active == 0}">
			<td>Idle</td>
		</c:if>
		<td>
			<button type="button" id="button-edit" value="${user.id}" class ="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>
			<button type="button" id="button-deactivate" value="${user.id}" class="btn btn-danger btn-xs"><i class="fa fa-times-rectangle-o"></i></button>
		</td>
	</tr>
</c:forEach>