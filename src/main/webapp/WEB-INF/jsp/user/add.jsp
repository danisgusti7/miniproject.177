<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-add-user" class="form-horizontal">
	<div class = "form-group">
		<label class="control-label col-md-3" for="role">Role</label>
		<div class="col-md-8">
			<select id="roleUser.id" name="roleUser.id" class="form-control">
				<c:forEach items="${roleList}" var ="role">
					<option value="${role.id}">${role.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class = "form-group">
		<label class="control-label col-md-3" for="username">Username</label>
		<div class="col-md-8">
			<input type="text" id="username" name="username" placeholder="Username" class="form-control"></input>
		</div>
	</div>
	<div class = "form-group">
		<label class="control-label col-md-3" for="password">Password</label>
		<div class="col-md-8">
			<input type="password" id="password" name="password" placeholder="Password" class="form-control"></input>
		</div>
	</div>
	<div class = "form-group">
		<label class="control-label col-md-3" for="retypePassword">Re-type Password</label>
		<div class="col-md-8">
			<input type="password" id="retypePassword" name="retypePassword" placeholder="Re-type Password" class="form-control"></input>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-add" class="btn btn-primary btn-sm">Save</button>
		<button type="button" id="button-submit-cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>

</form>