<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-edit-user" class="form-horizontal">
<input type="hidden" id="id" name="id" value="${user.id}"/>
	<div class = "form-group">
		<label class="control-label col-md-3" for="role">Role</label>
		<div class="col-md-8">
			<select id="roleUser.id" name="roleUser.id" class="form-control">
			<option selected value="${user.roleUser.id}">${user.roleUser.name}</option>
				<c:forEach items="${roleList}" var ="role">
					<option value="${role.id}">${role.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label class="control-label col-md-3" for="username">Username</label>
		<div class="col-md-8">
			<input type="text" id="username" name="username" value="${user.username}" class="form-control"/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="password">Password</label>
		<div class="col-md-8">
			<input type="text" id="password" name="password" value="${user.password}" class="form-control"/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="retypePassword">Password</label>
		<div class="col-md-8">
			<input type="text" id="retypePassword" name="retypePassword" value="${user.password}" class="form-control"/>
		</div>
	</div>
		<div class="form-group">
		<label class="control-label col-md-3" for="mobileFlag">Mobile Flag</label>
		<div class="col-md-8">
			<input type="radio">True</input>
			<input type="radio">False</input>
		</div>
	</div>
		</div>
		<div class="form-group">
		<label class="control-label col-md-3" for="mobileToken">Mobile Token</label>
		<div class="col-md-8">
			<input type="text" id="mobileToken" name="mobileToken" class="form-control"/>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-edit" class="btn btn-primary btn-sm">Edit</button>
		<button type="button" id="button-cancel-edit" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
	
</form>