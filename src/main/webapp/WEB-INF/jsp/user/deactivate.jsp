<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-deactivate-user" class="form-horizontal">
<input type="hidden" id="id" name="id" value="${user.id}"/>
<input type="hidden" id="roleUser.id" name="roleUser.id" value="${user.roleUser.id}"/>
	<div class = "form-group">
		<label class="control-label col-md-3" for="role">Role</label>
		<div class="col-md-8">
			<select id="role.id" name="role.id" class="form-control" disabled>
			<option selected value="${user.roleUser.id}">${user.roleUser.name}</option>
				<c:forEach items="${roles}" var ="role">
					<option value="${role.id}">${role.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label class="control-label col-md-3" for="username">Username</label>
		<div class="col-md-8">
			<input type="text" id="username" name="username" value="${user.username}" class="form-control" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="password">Password</label>
		<div class="col-md-8">
			<input type="text" id="password" name="password" value="${user.password}" class="form-control" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3" for="status">Status</label>
		<div class="col-md-8">
			<c:if test="${user.active == 0}">
				<td class="form-control">Idle</td>
			</c:if>
			<c:if test="${user.active == 1}">
				<td class="form-control">Active</td>
			</c:if>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" id="button-submit-edit" class="btn btn-primary btn-sm">Deactivate</button>
		<button type="button" id="button-cancel-edit" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
	
</form>