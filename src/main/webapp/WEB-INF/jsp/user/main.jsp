<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">User</h3>
		<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 250px;">
				<input type="text" id="usernameSearch" name="usernameSearch" placeholder="Search by Username" class="form-control pull-right"/>
				<div class="input-group-btn">
					<button type="button" id="button-search" class="btn btn-default"><i class="fa fa-search"></i></button>
					<button type="button" id="button-add-user" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</button>
				
				</div>
			</div>
				
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="table-user">
		<thead>
			<tr>
				<td>No</td>
				<td>Username</td>
				<td>Role</td>
				<td>Status</td>
				<td>Action</td>
			</tr>

		</thead>
			
			<tbody id="list-user">
			
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div> <!-- //POPUP -->


<script>
listUser();
function listUser(){
	$.ajax({
		url:"user/list.html",
		type: "POST",
		dataType: "html",
		success: function(result) {
			$("#list-user").html(result);
		
		}
	});
}
	
	$(document).ready(function(){
		$("#button-add-user").on("click", function(){
			$.ajax({
				url:"user/add.html",
				type: "POST",
				dataType: "html",
				success: function(result) {
					$("#modal-input").find(".modal-title").html("Add User");
					$("#modal-input").find(".modal-body").html(result);	
					$("#modal-input").modal("show");
				}
			});
		});
		
		$("#modal-input").on("submit", "#form-add-user", function(){
			var username = $("#modal-input").find("#username").val();
			var password = $("#modal-input").find("#password").val();
			var retypePassword = $("#modal-input").find("#retypePassword").val();
			//Must at least contain 1 letter and 1 number, also must be at least 8 characters
			var validUsername = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){8,}$/;
			//Must at least contain 1 letter and 1 number + 1 Uppercase letter
			var validPassword = /^(?=.*[A-Z].*)(?=.*[\d].*)[a-zA-Z\d]+$/;
			
			if(username == "" || password == "" || retypePassword == ""){
				alert("Fields cannot be empty");
			}else if(!(username.match(validUsername))){
				alert("Username must be a combination of letters and numbers and at least 8 characters long");
			}else if(password != retypePassword){
				alert("Password and Re-type Password has to match");
			}else if(!(password.match(validPassword)) && !(retypePassword.match(validPassword))){
				alert("Password must be a combination of letters and numbers and must contain an Uppercase letter");
			}else{
				$.ajax({
					url:"user/create.json", //menambah data ke back-end
					type: "POST",
					dataType: "json",
					data: $(this).serialize(),
					success: function(result) {
						$("#modal-input").modal("hide");
						alert("User Saved");
						listUser();
					}
				});	
			}
			return false;
		});
		
		
		$("#button-search").on("click", function(){
			var usernameSearch = $("#usernameSearch").val();
			$.ajax({
				url: "user/search/username.html",
				type: "POST",
				dataType: "html",
				data:{usernameSearch: usernameSearch},
				success: function(result){
					$("#list-user").html(result);
				}
			});
			return false;
			
		});
		
		$("#list-user").on("click", "#button-edit", function(){
			var idEdit = $(this).val();
			$.ajax({
				url: "user/edit.html",
				type: "POST",
				dataType: "html",
				data: {id: idEdit},
				success: function(result){
					$("#modal-input").find(".modal-title").html("Edit User");
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal("show");
				}
				
			});
		});
		
		$("#modal-input").on("submit", "#form-edit-user", function(){
			var username = $("#modal-input").find("#username").val();
			var password = $("#modal-input").find("#password").val();
			var retypePassword = $("#modal-input").find("#retypePassword").val();
			//Must at least contain 1 letter and 1 number, also must be at least 8 characters
			var validUsername = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){8,}$/;
			//Must at least contain 1 letter and 1 number + 1 Uppercase letter
			var validPassword = /^(?=.*[A-Z].*)(?=.*[\d].*)[a-zA-Z\d]+$/;
			
			if(username == "" || password == "" || retypePassword == ""){
				alert("Fields cannot be empty");
			}else if(!(username.match(validUsername))){
				alert("Username must be a combination of letters and numbers and at least 8 characters long");
			}else if(password != retypePassword){
				alert("Password and Re-type Password has to match");
			}else if(!(password.match(validPassword)) && !(retypePassword.match(validPassword))){
				alert("Password must be a combination of letters and numbers and must contain an Uppercase letter");
			}else{
				$.ajax({
					url: "user/edit/save.json",
					type: "POST",
					dataType: "json",
					data: $(this).serialize(),
					success: function(result){
						$("#modal-input").modal("hide");
						alert("Changes has been saved");
						listUser();
					}
				});
			}
			return false;
		});
		
		$("#list-user").on("click", "#button-deactivate", function(){
			var idDeactivate = $(this).val();
			$.ajax({
				url: "user/deactivate.html",
				type: "POST",
				dataType: "html",
				data: {id: idDeactivate},
				success: function(result){
					$("#modal-input").find(".modal-title").html("Deactivate This User?");
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal("show");
				}
			});
			
		});
		
		$("#modal-input").on("submit", "#form-deactivate-user", function(){
			$.ajax({
				url: "user/deactivate/save.json",
				type: "POST",
				dataType: "json",
				data: $(this).serialize(),
				success: function(result){
					$("#modal-input").modal("hide");
					alert("Status Has Changed");
					listUser();
				}
			});
			return false;
			
		});

		
	});	
	

	
</script>