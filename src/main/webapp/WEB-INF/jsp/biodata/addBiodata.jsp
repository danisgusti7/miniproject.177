<html>
	<div class="modal fade" id="addBiodata" role="dialog"
	data-parsley-validate>
	<div class="modal-dialog">

		<!-- modal content  -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">ADD BIODATA</h3>
			</div>
			<div class="modal-body">
				<form class="form-group">
					<div class="form-group">
						<input type="text" id="biodataName" placeholder="Name" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="lastEducation" placeholder="Last Education" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="educationLevel" placeholder="Education Level" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="graduationYear" placeholder="Graduation Year" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="majors" placeholder="Major" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="gpa" placeholder="GPA" class="form-control">
					</div>
				</form>
			</div>
			<div>
				<div class="modal-footer">
					<button type="button" id="btnSaveBiodata" class="btn btn-primary">Save</button>
					<button type="button" id="btnCanceBiodata" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
</html>