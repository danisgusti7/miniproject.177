<html>
	<div class="modal fade" id="modalDeleteBiodata" role="dialog"
	data-parsley-validate>
	<div class="modal-dialog">

		<!-- modal content  -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Delete this biodata ?</h3>
			</div>
			<div class="modal-body">
				<form class="form-group">
					<div class="form-group">
						<input type="text" id="deleteBiodataName" placeholder="Name" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="deleteLastEducation" placeholder="Last Education" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="deleteEducationLevel" placeholder="Education Level" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="deleteGraduationYear" placeholder="Graduation Year" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="deletemajors" placeholder="Major" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="deletegpa" placeholder="GPA" class="form-control">
					</div>
				</form>
			</div>
			<div>
				<div class="modal-footer">
					<button type="button" id="btnDeleteBiodata" class="btn btn-primary">Delete</button>
					<button type="button" id="btnCanceBiodata" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
</html>