<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<div class="modal fade" id="modalEditBiodata" role="dialog"
	data-parsley-validate>
	<div class="modal-dialog">

		<!-- modal content  -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">EDIT BIODATA</h3>
			</div>
			<div class="modal-body">
				<form class="form-group">
					<div class="form-group">
						<input type="text" id="editBiodataName" placeholder="Name" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="editLastEducation" placeholder="Last Education" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="editEducationLevel" placeholder="Education Level" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="editGraduationYear" placeholder="Graduation Year" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="editmajors" placeholder="Major" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="editgpa" placeholder="GPA" class="form-control">
					</div>
					<label>Gender 	:</label>
					<label class="radio-inline"><input type="radio" name="optradio">Laki-aki</label>
					<label class="radio-inline"><input type="radio" name="optradio">Perempuan</label>
					<div>
					<select id="biodata-select">
							<option>Select Test Type</option>
						<c:forEach items="${testType}" var="test">
							<option value="${test.id }">${test.name}</option>
						</c:forEach>
					</select>
					</div>
				</form>
			</div>
			<div>
				<div class="modal-footer">
					<button type="button" id="btnUpdateBiodata" class="btn btn-primary">Update</button>
					<button type="button" id="btnCanceBiodata" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
</html>