<html>
<div class="row">
	<div class="col-sm-12">

		<div class="box box-primary">
			<div class="box-header with-border">
				<h2>BIODATA</h2>
			</div>

			<div class="box-body">
				<div>
					<button type="button" class="btn btn-info" data-toggle="modal"
						data-target="#addBiodata">Add Biodata</button>
				</div>
				<br>
				<div>
					<table id="tableBiodata" class="table table-stripe cell-border" ></table>
				</div>
			</div>
		</div>

	</div>
</div>
<%@include file="addBiodata.jsp"%>
<%@include file="editBiodata.jsp"%>
<%@include file="deleteBiodata.jsp"%>
<script type="text/javascript">
	
	$(function(){
		listBiodata();
		$("#btnSaveBiodata").click(function(){
			saveBiodata();
			$('#tableBuku').DataTable().destroy();
			$('#tableBuku').empty();
			listBiodata();
			
		});
	})
	
	function saveBiodata(){
		$.ajax({
			type 	: "POST",
			url		: "${pageContext.request.contextPath}/biodata/save.json",
			data	: {
				biodataName 	: $("#biodataName").val(),
				lastEducation	: $("#lastEducation").val(),
				educationLevel	: $("#educationLevel").val(),
				graduationYear	: $("#graduationYear").val(),
				majors			: $("#majors").val(),
				gpa				: $("#gpa").val(),
			 },
			 success	: function(d){
				if (d.success) {
					alert("insert data berhasil");
				}		
			}
		});
		 $("#addBiodata").modal("hide");
	}
	
	function listBiodata(){
		$.ajax({
			type	: "POST",
			url		: "${pageContext.request.contextPath}/biodata/list.json",
			success	: function(d){
				if (d.success == true){
					var dataSet = d.listBiodata
					
					$('#tableBiodata').DataTable({
						data	: dataSet,
						/* paging	: false,
						search	: false, */
						 destroy: true,
						 paging: true,
						 lengthChange: true,
						 searching: true,
						 ordering: true,
						 info: true,
						 autoWidth: true,
						 
						columns	: [{
							title	: 'Name',
							data	: 'name'
						}, {
							title	: 'Majors',
							data	: 'majors'
						}, {
							title	: 'GPA',
							data	: 'gpa'
						}, {
							title	: 'Action',
							data	: null,
							clasName : "center",
							defaultContent: '<a class="fa fa-pencil" data-toggle="modal" onclick=editBiodata(this) class="editor_edit"></a> / <a class="fa fa-trash-o" onclick=deleteBiodata(this)  class="editor_remove"></a>'
						}]
					});
				}
			}
		});
	}
	
	function editBiodata(element){
		//ambil elemen table nya
		var tableId = $(element).closest('table').attr('id');
		//ambil element tr nya
		var tr = $(element).closest('tr');
		//ambil data satu row
		// coba aja nilai datanya dikeluarin atau di dibug
		var data = $('#tableBiodata').DataTable().row(tr).data();
		//variabel untuk menampung id yang akan di update
		var tangkapId = data.id;
		// debugger; // bisa pakai ini, jadi mirip di java. nanti aplikasi akab berhenti
		// ketika sampai titik ini dan silahkan lihat di insect element
		// console.log(data); // lihat di console log nilai datanya
		// data digunakan untuk mengisi modal form
		// sekaligus nambah atribute biar readonly
		
		$('#editBiodataName').val(data.name);
		$('#editLastEducation').val(data.lastEducation);
		$('#editEducationLevel').val(data.educationalLevel);
		$('#editGraduationYear').val(data.graduationYear);
		$('#editmajors').val(data.majors);
		$('#editgpa').val(data.gpa);
		
		$('#modalEditBiodata').modal('show');
		
		$('#btnUpdateBiodata').click(function(){
			$.ajax({
				type	: "POST",
				url		: "${pageContext.request.contextPath}/biodata/update.json",
				data	: {
					id	: tangkapId,
					biodataName		: $("#editBiodataName").val(),
					lastEducation	: $("#editLastEducation").val(),
					educationLevel	: $("#editEducationLevel").val(),
					graduationYear	: $("#editGraduationYear").val(),
					majors			: $("#editmajors").val(),
					gpa				: $("#editgpa").val(),
				},
				success : function(d) {
					if (d.success) {
						alert("Data Berhasil Di Update");
						$("#tableBiodata").DataTable().clear();
						$("#tableBiodata").DataTable().destroy();
						$("#modalEditBiodata").modal("hide");
					} else {
						alert("Data Gagal Di Update");
						
					}
					listBiodata();
				}
			});
		});
	}
	
	function deleteBiodata(element){
		var idHapus = $(element).closest('table').attr('id');
		var tr		= $(element).closest('tr');
		var data	= $('#tableBiodata').DataTable().row(tr).data();
		var tangkapId = data.id;
		
		$('#deleteBiodataName').val(data.name).attr("readOnly", true);
		$('#deleteLastEducation').val(data.lastEducation).attr("readOnly", true);
		$('#deleteEducationLevel').val(data.educationalLevel).attr("readOnly", true);
		$('#deleteGraduationYear').val(data.graduationYear).attr("readOnly", true);
		$('#deletemajors').val(data.majors).attr("readOnly", true);
		$('#deletegpa').val(data.gpa).attr("readOnly", true);
		
		$('#modalDeleteBiodata').modal('show');
		$('#btnDeleteBiodata').click(function () {
			$.ajax({
				type	: "POST",
				url		: "${pageContext.request.contextPath}/biodata/delete.json",
				data	: {
						id 	: tangkapId,
				},
				success : function(d){
					if(d.success){
						alert('Data Deleted Successfully');
						$('#tableBiodata').DataTable().clear();
						$('#tableBiodata').DataTable().destroy();
						$("#modalDeleteBiodata").modal("hide");
					} else {
						alert('error');
					}
					listBiodata();
				}
			})
		})
	}
</script>
 </html>
