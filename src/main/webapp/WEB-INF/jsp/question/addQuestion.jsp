<form action="#" method="post" id="form-add" class="form-horizontal">
	<div class="form-group">
		<!-- <label class="control-label col-md-3" for="question"></label> -->
		<div class="col-md-8">
			<textarea rows="5" cols="10" id="question" name="questionContent" class="form-control" placeholder="Type your question"></textarea>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="saveQuestion" class="btn btn-primary btn-sm">Save</button>
		<button type="submit" id="cancelQuestion" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>