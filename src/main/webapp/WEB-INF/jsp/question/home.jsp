<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Question</h3>
		<div class="box-tools">
			<div class="input-group input-group-sm" style="width:250px;">
				<input type="text" id="searchQuestion" name="searchQuestion" placeholder="Search by question"/>
				<div class="input-group-btn">
					<button type="button" id="button-search" class="btn btn-default"><i class="fa fa-search"></i></button>
					<button type="button" id="button-add" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i>Add</button>						
				</div>
			</div>
		</div>
				</div>
	<div class="box-body">
		<table class="table" id="table-question">
			<thead>
				
				<tr>
					<th>Question</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-question">
				
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<script>
	
	listQuestion();
	function listQuestion(){
		$.ajax({
				url:"question/list.html",
				type: "get",
				dataType: "html",
				success: function(result) {
					$("#list-question").html(result);
				}
			
		});
	}

	$(document).ready(function(){
		$("#button-add").on("click", function(){
			$.ajax({
				url:"question/addQuestion.html",
				type: "get",
				dataType: "html",
				success: function(result) {
					$("#modal-input").find(".modal-title").html("Add Question");
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal("show");
				}
			});
		});
	
		
	$("#modal-input").on("submit", "#form-add", function(){
		var validationQuestionContent = $("#modal-input").find("#question").val();
		if(validationQuestionContent == ""){
			alert("Question must be filled out!");
		}else{
			$.ajax({
				url:"question/create.json", //menambah data ke back-end
				type: "get",
				dataType: "json",
				data:$(this).serialize(),
				success: function(result) {
					$("#modal-input").modal("hide");
					alert("Question successfully added!");
					listQuestion();
				}
			});
		}
		
		
		return false;
	});
	
	$("#button-search").on("click", function(){
		var searchQuestion = $("#searchQuestion").val();
		$.ajax({
			url:"question/search/getQuestion.html",
			type:"get",
			dataType:"html",
			data:{searchQuestion:searchQuestion},
			success: function(result){
				$("#list-question").html(result);
			}
		});
		return false;
	});
	
	$("#list-question").on("click", "#button-delete",function(){
		var idDelete = $(this).val();
		$.ajax({
			url:"question/delete.html",
			type:"get",
			dataType:"html",
			data: {id:idDelete},
			success: function(result){
				$("#modal-input").find(".modal-title").html("Do you want to delete question?");
				$("#modal-input").find(".modal-body").html(result);
				$("#modal-input").modal("show");
			}
		});
	});
	
	$("#modal-input").on("submit","#form-delete", function(){
		$.ajax({
			url:"question/delete/save.json",
			type:"get",
			dataType:"json",
			data:$(this).serialize(),
			success: function(result){
				$("#modal-input").modal("hide");
				alert("Question successfully deleted!");
				listQuestion();
				
			}
		});
		return false;
	});
	
});
</script>
</body>
</html>