<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<h4>Are you sure to delete the question ?</h4>
	<form action="#" method="get" id="form-delete" class="form-horizontal">
		<div class="form-group">
			<div class="col-md-10">
				<input type="hidden" id="id" name="id" value="${question.id}" />
				<textarea rows="8" cols="10" id="question" name="question" class="form-control" disabled="disabled">${question.question}</textarea> <br/>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" id="btn-submit" class="btn btn-primary btn-sm">Delete</button>
			<button type="submit" id="cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
		</div>
	</form>