<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form action="#" method="get" id="form-add-version" class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-3" for="versionQuestion">Version</label>
		<div class="col-md-8">
			<input type ="text" id="versionQuestion" name="versionNum" class="form-control" value="${newVersion}" readonly="readonly"></input><br>
			<button type="button" id="button-add-question" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i>Question</button>
			<br>
			
			<table class="table">
					<thead>
						<tr>
							<th>Question</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="list-question">
						<c:forEach items="${versions}" var="data">
							<tr>
								<td>${data.question}</td>
								<td><button type="button" value="${data.id}" class="btn btn-danger btn-xs btn-delete-question"><i class="fa fa-trash"></i></button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="saveVersion" class="btn btn-primary btn-sm" >Save</button>
		<button type="submit" id="cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>