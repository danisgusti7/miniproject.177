<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form action="#" method="get" id="form-add-selection" class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-3" for="versionQuestion">List Question</label>
		<div class="col-md-8">
			<select id="question-select">
				<c:forEach items="${questions}" var="data">
					<option value="${data.id}">${data.question}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" id="save" class="btn btn-primary btn-sm">Add</button>
		<button type="button" id="cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
</form>