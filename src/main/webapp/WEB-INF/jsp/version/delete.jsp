<h4>Are you sure to delete the version ?</h4>
	<form action="#" method="post" id="form-delete-version" class="form-horizontal">
		<div class="form-group">
			<div class="col-md-10">
				<input type="hidden" id="id" name="id" value="${version.id}" />
				<textarea rows="8" cols="10" id="version" name="version" class="form-control" disabled="disabled">${version.version}</textarea> <br/>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" id="btn-submit" class="btn btn-primary btn-sm">Delete</button>
		</div>
	</form>