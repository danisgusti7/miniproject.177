<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Version</h3>
		<div class="box-tools">
				<button type="button" id="button-add" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i>Add</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="table-version">
			<thead>
				<tr>
					<th>Version</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-version">
				
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-input2">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<script>

	listVersion();
	function listVersion(){ //membuat fungsi listVersion()
		$.ajax({
				url:"version/list.html", //memanggil file jsp bernama list pada folder version
				type: "get", //jenis tampilan web html
				dataType: "html", //jenis data
				success: function(result) { //jika proses pemanggilan file sukses, maka
					$("#list-version").html(result); //mengeluarkan hasil di bagian tbody ber-id list-version
				}
			
		});
	}
	
	listQuestion();
	function listQuestion(){
		$.ajax({
				url:"${pageContext.request.contextPath}/question/list.html",
				type: "post",
				dataType: "html",
				success: function(result) {
					$("#list-question").html(result);
				}
			
		});
	}

	$(document).ready(function(){ //saat function html ready???
		$("#button-add").on("click", function(){ //saat button-add di klik, function() dijalankan
			$.ajax({
				url:"version/addVersion.html",
				type: "post",
				dataType: "html",
				success: function(result) {
					$("#modal-input").find(".modal-title").html("Add Version");
					$("#modal-input").find(".modal-body").html(result); //memanggil modal-body sbg lokasi untuk menampilkan isi file addVersion 
					$("#modal-input").modal("show"); //menampilkan modal input
				}
			});
		});
	
	
		$("#modal-input").on("click","#button-add-question", function(){ //pada modal-input, saat button add question diklik, function() dijalankan			
			$.ajax({
				url:"version/addSelection.html",
				type: "post",
				dataType: "html",
				success: function(result) {
					$("#modal-input2").find(".modal-title").html("Select Question");
					$("#modal-input2").find(".modal-body").html(result);
					$("#modal-input2").modal("show");//menampilkan modal-input2
				}
			});
			
		});
		
		$("#modal-input2").on("click", "#save", function(){ //saat tombol add diklik
			var selected = $("#modal-input2").find("#question-select option:selected"); //mmebuat var selected yg menyimpan data terpilih
			var idQuestion = selected.val(); //menampung id question terpilih dari var selected
			var textQuestion = selected.text(); //menampung text dari data yang dipilih
			var items = '<tr>'+
			'<td><input type="hidden" class="form-control txt-question" value="'+idQuestion+'" />'+ textQuestion +'</td>'+
			'<td><button type="button" class="btn btn-danger btn-xs btn-delete-question"><i class="fa fa-trash"></i></button></td>'+
			'</tr>';
			$("#modal-input").find("#list-question").append(items); //pada modal-input utama, question terpilih ditambahkan ke list-question 
			$("#modal-input2").modal("hide");//modal-input kedua di close
		});
		
		
		$("#modal-input").on("submit", "#form-add-version", function(){
			var version = $('#modal-input').find('#versionQuestion').val();
			
			var versionNum = {
					version: version,
					versionDetails: []
			};
			
			//list question
			var oTable =$("#modal-input").find('#list-question tr');
			var listQuestion = [];
			
			$.each(oTable, function(index, value){
				var question = $(this).find('.txt-question').val();
				var date = new Date();
				
				var versionDetail = {
						
						createdOn: date,
						questionId :{
							id: question
						},
						versionId:{
							id: version
						}
				};
				versionNum.versionDetails.push(versionDetail);
			});
			console.log(versionNum);
			
			var validationVersion = $("#modal-input").find("#list-question").val();
			if(validationVersion == ""){
				alert("Version must be have questions!");
			}else{	
				jQuery.ajax({
					url:"${pageContext.request.contextPath}/version/create.json",
					type: "post",
					beforeSend: function(){
						console.log(versionNum);
					},
					contentType: 'application/json',
					dataType: "json",
					data: JSON.stringify(versionNum),
					success: function(data) {
						console.log(data);
						$("#modal-input").modal("hide");
						alert("Version Added!");
						listVersion();
					}
				});
			}
			return false;
		});

		$("#modal-input").on("click", ".btn-delete-question", function(){
			$(this).parent().parent().remove();// parent() => return a set containing doc ("html")
		});
		
		/* jQuery object represent set of DOM element, si parent() ini melintasi induk dari masing2
		element dan mebuat object jQuery baru yg cocok */
	
	$("#list-version").on("click", "#button-delete",function(){
		var idDelete = $(this).val();//get current value
		$.ajax({
			url:"version/delete.html",
			type:"post",
			dataType:"html",
			data: {id:idDelete},
			success: function(result){
				$("#modal-input").find(".modal-title").html("Do you want to delete version?");
				$("#modal-input").find(".modal-body").html(result);
				$("#modal-input").modal("show");
			}
		});
	});
	
	$("#modal-input").on("submit","#form-delete-version", function(){
		$.ajax({
			url:"version/delete/saveVersion.json",
			type:"post",
			dataType:"json",
			data:$(this).serialize(),
			success: function(result){
				$("#modal-input").modal("hide");
				alert("Version deleted!");
				listVersion();
				
			}
		});
		return false;
	});
});
</script>