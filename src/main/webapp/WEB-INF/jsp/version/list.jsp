<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
	
	<c:forEach items="${versions}" var="data">
		<tr>
		<td>${data.version}</td>
		<td><button type="button" id="button-delete" value="${data.id}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></td>	
		</tr>
	</c:forEach>
	