<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Feedback</h3>
	</div>
	<div class="box-body">
		<table class="table" id="table-feedback">
			<thead>
				<tr>
					<th>Test Type</th>
				</tr>
				<tr>
					<th>
						<form method="GET" id="form-select-test">
							<select id="testType" name="testType" class="form-control">
								<option value="">---Select Test Type---</option>
								<c:forEach items="${tests}" var="data">
									<option value="${data.id}">${data.name}</option>
								</c:forEach>
							</select>
						</form>
					</th>
				</tr>
			</thead>
			<tbody id="list-feedback">
				<!-- <input type="hidden" value="" id="versionId" name="versionId"> -->
				<c:forEach items="${questionList}" var="data">
					<tr>
					<td>${data.question}</td>
					</tr>
					<tr>
					<td><textarea rows="5" cols="10" id="jsonFeedback" name="jsonFeedback" class="form-control"></textarea>
					 </td>
					 </tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="box-footer">
		<button type="button" id="button-save" class="btn btn-primary btn-sm">Save</button>
		<button type="button" id="button-cancel" class="btn btn-primary btn-sm">Cancel</button>						
	</div>
</div>

<script>

	$(document).ready(function(){
		$("#button-save").on("click", function(){
			$.ajax({
				url:"feedback/create.json",
				type: "get",
				dataType: "json",
				data:$(this).serialize(),
				success: function(result) {
					alert("Data successfully submitted!");
				}
			});
			return false;
		});
		
	});
</script>