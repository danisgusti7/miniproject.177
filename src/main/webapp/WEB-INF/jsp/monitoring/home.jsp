<html>
<div class="row">
	<div class="col-sm-12">

		<div class="box box-primary">
			<div class="box-header with-border">
				<h2>IDLE MONITORING</h2>
			</div>

			<div class="box-body">
				<div>
					<button type="button" class="btn btn-info" data-toggle="modal"
						data-target="#addMonitoring">+</button>
				</div>

			</div>
		</div>

	</div>
</div>

<%@include file="addMonitoring.jsp"%>

<script type="text/javascript">

$(function(){
	$("#btnSaveIdle").click(function(){
		saveIdle();
	});
})

 function saveIdle() {
	 $.ajax({
		 type 	: "POST",
		 url	: "${pageContext.request.contextPath}/monitoring/save",
		 data	: {
			 idleName	: $("#idleName").val(),
			 idleDate	: $("#idleDate").val(),
			 idleLastProjectAt : $("#idleLastProjectAt").val(),
			 idleReason	: $("#idleReason"),
			 success	: function(data){
				 console.log(data);
			 }, error : function(){
				 alert("insert data faild");
			 }
		 }
	 });
 }
 
</script>
</html>