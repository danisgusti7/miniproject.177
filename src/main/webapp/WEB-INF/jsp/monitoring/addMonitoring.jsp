<html>
<div class="modal fade" id="addMonitoring" role="dialog"
	data-parsley-validate>
	<div class="modal-dialog">

		<!-- modal content  -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">INPUT IDLE</h3>
			</div>
			<div class="modal-body">
				<form class="form-group">
					<div class="form-group">
						<input type="text" id="idleName" placeholder="Name" class="form-control">
					</div>
					<div class="form-group">
						<input type="date" id="idleDate" placeholder="Idle Date" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="idleLastProjectAt" placeholder="Last Project At" class="form-control">
					</div>
					<div class="form-group">
						<input type="text" id="idleReason" placeholder="Idle Reason" class="form-control">
					</div>
				</form>
			</div>
			<div>
				<div class="modal-footer">
					<button type="button" id="btnSaveIdle" class="btn btn-primary">Save</button>
					<button type="button" id="btnCancelIdle" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>

	</div>
</div>
</html>