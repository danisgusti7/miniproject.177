<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Menu Access</h3>
		<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 250px;">
				<select id="roleSearch" name="roleSearch" class="form-control pull-right">
					<option value="">----Select Role----</option>
					<c:forEach items="${roleList}" var ="role">
						<option value="${role.id}">${role.name}</option>
					</c:forEach>
				</select>
				<div class="input-group-btn">
					<button type="button" id="button-search" class="btn btn-default"><i class="fa fa-search"></i></button>	
					<button type="button" id="button-add-menuAccess" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Add</button>
				</div>
			</div>

		</div>
	</div>
	<div class="box-body">
		<table class="table" id="table-menuAccess">
		<thead>
			<tr>
				<td>No</td>
				<td>Role</td>
				<td>Menu</td>
				<td>Action</td>
			</tr>
		</thead>
			
			<tbody id="list-menuAccess">
			
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div> <!-- //POPUP -->




<script>
listMenuAccess();
function listMenuAccess(){
	$.ajax({
		url:"menuAccess/list.html",
		type: "POST",
		dataType: "html",
		success: function(result) {
			$("#list-menuAccess").html(result);
		
		}
	});
}

	$(document).ready(function(){
		$("#button-add-menuAccess").on("click", function(){
			$.ajax({
				url:"menuAccess/add.html",
				type: "POST",
				dataType: "html",
				success: function(result) {
					$("#modal-input").find(".modal-title").html("Add Menu Access");
					$("#modal-input").find(".modal-body").html(result);	
					$("#modal-input").modal("show");
				}
			});
		});
		
		$("#modal-input").on("submit", "#form-add-menuAccess", function(){
			$.ajax({
				url:"menuAccess/create.json", //menambah data ke back-end
				type: "POST",
				dataType: "json",
				data: $(this).serialize(),
				success: function(result) {
					$("#modal-input").modal("hide");
					alert("Menu Access Saved");
					listMenuAccess();
				}
			});
			return false;
		});
		
		$("#button-search").on("click", function(){
			var roleSearch = $("#roleSearch").val();
			$.ajax({
				url: "menuAccess/search/role.html",
				type: "POST",
				dataType: "html",
				data: {roleSearch : roleSearch},
				success: function(result){
					$("#list-menuAccess").html(result);
				}
			});
			return false;
		});
		
		$("#list-menuAccess").on("click", "#button-delete", function(){
			var idDelete = $(this).val();
			$.ajax({
				url: "menuAccess/delete.html",
				type: "POST",
				dataType: "html",
				data: {id: idDelete},
				success: function(result){
					$("#modal-input").find(".modal-title").html("Delete This Menu Access?");
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal("show");
				}
				
			});
			
		});
		
		$("#modal-input").on("submit", "#form-delete-menuAccess", function(){
			$.ajax({
				url: "menuAccess/delete/save.json",
				type: "POST",
				dataType: "json",
				data: $(this).serialize(),
				success: function(result){
					$("#modal-input").modal("hide");
					alert("Menu Access Has Been Deleted");
					listMenuAccess();
				}
			});
			return false;
		});
		
		
	});

</script>