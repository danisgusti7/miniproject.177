<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-add-menuAccess" class="form-horizontal">
	<div class = "form-group">
		<label class="control-label col-md-3" for="role">Role</label>
		<div class="col-md-8">
			<select id="role.id" name="roleMenuAccess.id" class="form-control">
				<c:forEach items="${roleList}" var ="role">
					<option value="${role.id}">${role.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class = "form-group">
		<label class="control-label col-md-3" for="menu">Menu</label>
		<div class="col-md-8">
			<select id="menu.id" name="menuMenuAccess.id" class="form-control">
				<c:forEach items="${menuList}" var ="menu">
					<option value="${menu.id}">${menu.title}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-add" class="btn btn-primary btn-sm">Save</button>
		<button type="button" id="button-submit-cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
	

</form>