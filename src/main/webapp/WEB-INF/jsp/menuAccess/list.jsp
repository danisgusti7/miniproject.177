<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach items="${menuAccessList}" var="menuAccess" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${menuAccess.roleMenuAccess.name}</td>
		<td>${menuAccess.menuMenuAccess.title}</td>
				<td>
			<button type="button" id="button-delete" value="${menuAccess.id}" class="btn btn-danger btn-xs"><i class="fa fa-times-rectangle-o"></i></button>
		</td>
	</tr>

</c:forEach>