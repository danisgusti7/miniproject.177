<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="#" method="POST" id="form-delete-menuAccess" class="form-horizontal">
<input type="hidden" id="id" name="id" value="${menuAccess.id}"/>
<input type="hidden" id="roleMenuAccess.id" name="roleMenuAccess.id" value="${menuAccess.roleMenuAccess.id}"/>
<input type="hidden" id="menuMenuAccess.id" name="menuMenuAccess.id" value="${menuAccess.menuMenuAccess.id}"/>
	<div class = "form-group">
		<label class="control-label col-md-3" for="role">Role</label>
		<div class="col-md-8">
			<select id="role.id" name="role.id" class="form-control" disabled>
			<option selected value="${menuAccess.roleMenuAccess.id}">${menuAccess.roleMenuAccess.name}</option>
				<c:forEach items="${roles}" var ="role">
					<option value="${role.id}">${role.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class = "form-group">
		<label class="control-label col-md-3" for="menu">Menu</label>
		<div class="col-md-8">
			<select id="menu.id" name="menu.id" class="form-control" disabled>
			<option selected value="${menuAccess.menuMenuAccess.id}">${menuAccess.menuMenuAccess.title}</option>
				<c:forEach items="${menus}" var ="menu">
					<option value="${menu.id}">${menu.title}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="button-submit-delete" class="btn btn-primary btn-sm">Delete</button>
		<button type="button" id="button-submit-cancel" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
	</div>
	

</form>