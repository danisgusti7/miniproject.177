<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Version</h3>
		<div class="box-tools">
				<button type="button" id="button-add" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i>Add</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="table-version">
			<thead>
				<tr>
					<th>Version</th>
				</tr>
			</thead>
			<tbody id="list-version">
				
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<!-- <h4 class="modal-title">list</h4> -->
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function(){
		
		$("#modal-input").on("click", "#save", function(){
			var idVersion = $(this).val();
			var textVersion = $(this).text();
			var items = '<tr>'+
			'<td><input type="text" class="form-control txt-question" value="'+idQuestion+'" />'+ textQuestion +'</td>'+
			'<td><button type="button" class="btn btn-danger btn-xs btn-delete-question"><i class="fa fa-trash"></i></button></td>'+
			'</tr>';
			$("#modal-input").find("#list-question").append(items); //pada modal-input utama, question terpilih ditambahkan ke list-question 
			$("#modal-input2").modal("hide");//modal-input kedua di close
		});
		
		
});
</script>