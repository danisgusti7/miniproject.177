package com.xbc.dao;

import java.util.List;

import com.xbc.model.MenuAccess;
import com.xbc.model.Role;

public interface MenuAccessDao {

	void create(MenuAccess menuAccess);

	List<MenuAccess> searchAll();

	List<MenuAccess> searchByRole(Role role);

	MenuAccess searchById(String id);

	void delete(MenuAccess menuAccess);



}
