package com.xbc.dao;

import com.xbc.model.VersionDetail;

public interface VersionDetailDao {

	void create(VersionDetail versionDetail);

}
