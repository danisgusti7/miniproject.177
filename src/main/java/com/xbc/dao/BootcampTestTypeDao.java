package com.xbc.dao;

import java.util.List;

import com.xbc.model.BootcampTestType;

public interface BootcampTestTypeDao {

	List<BootcampTestType> getAllTestType();
	
}
