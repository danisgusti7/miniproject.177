package com.xbc.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.VersionDetailDao;
import com.xbc.model.VersionDetail;

@Repository
public class VersionDetailDaoImpl implements VersionDetailDao {

	@Autowired
	SessionFactory sessionfactory;
	
	@Override
	public void create(VersionDetail versionDetail) {
		Session session = sessionfactory.getCurrentSession();
		session.save(versionDetail);
		
	}

}
