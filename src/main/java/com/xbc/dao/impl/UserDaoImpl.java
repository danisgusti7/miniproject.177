package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.UserDao;
import com.xbc.model.Role;
import com.xbc.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchAll() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession(); 
		List<User> userList = new ArrayList<User>();
		userList = session.createQuery("select u from User u where u.active=1 order by u.username").list();
		return userList;
	}

	@Override
	public void create(User user) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	@Override
	public List<User> searchByUsername(String username) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select u from User u where u.active=1 and lower(u.username) like lower('%"+username+"%')";
		List<User> userList = new ArrayList<User>();
		userList = session.createQuery(query).list();
		return userList;
	}

	@Override
	public User searchById(String id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select u from User u where u.id='"+id+"'";
		User user = new User();
		user = (User) session.createQuery(query).getSingleResult();
		return user;
	}

	@Override
	public void edit(User user) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "update User u set u.username =:username, u.roleUser.id =:roleUser, u.password =:password, "
				+ "u.mobileFlag =:mobileFlag, u.mobileToken =:mobileToken, u.modifiedBy =:modifiedBy, u.modifiedOn =:modifiedOn"
				+ " where u.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", user.getId());
		query.setParameter("username", user.getUsername());
		query.setParameter("roleUser", user.getRoleUser().getId());
		query.setParameter("password", user.getPassword());
		query.setParameter("mobileFlag", user.getMobileFlag());
		query.setParameter("mobileToken", user.getMobileToken());
		query.setParameter("modifiedBy", user.getModifiedBy());
		query.setParameter("modifiedOn", user.getModifiedOn());
		query.executeUpdate();
	}

	@Override
	public void deactivate(User user) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "update User u set u.modifiedBy =:modifiedBy, u.modifiedOn =:modifiedOn, u.active =:active"
				+ " where u.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", user.getId());
		query.setParameter("modifiedBy", user.getModifiedBy());
		query.setParameter("modifiedOn", user.getModifiedOn());
		query.setParameter("active", user.getActive());
		query.executeUpdate();
	}

	@Override
	public User getByUsername(String username) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select u from User u where u.username='"+username+"'";
		User user = new User();
		user = (User) session.createQuery(query).getSingleResult();
		return user;
	}


	
	



}
