package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.TestDao;
import com.xbc.model.Test;

@Repository
public class TestDaoImpl implements TestDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Test> getTest() {
		Session session = sessionFactory.getCurrentSession();
		String query = "select t from Test t";
		List<Test> tests = new ArrayList<Test>();
		tests = session.createQuery(query).list();
		return tests;
	}

	@Override
	public Test getById(Long testFeedback) {
		Session session = sessionFactory.getCurrentSession();
		String query = "select t from Test t where id='"+testFeedback+"'";
		Test test = new Test();
		test = (Test) session.createQuery(query).getSingleResult();
		return test;
	}

}
