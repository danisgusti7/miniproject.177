package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.QuestionDao;
import com.xbc.model.Question;

@Repository
public class QuestionDaoImpl implements QuestionDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public void create(Question question) {
		Session session = sessionFactory.getCurrentSession();
		session.save(question);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Question> getAllQuestions() {
		Session session = sessionFactory.getCurrentSession();
		List<Question> questions = new ArrayList<Question>();
		questions = session.createQuery("select q from Question q where isDelete='0'").list();
		return questions;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Question> searchByQuestion(String question) {
		Session session = this.sessionFactory.getCurrentSession();
		String query = "select q from Question q where q.question like '%"+question+"%' AND isDelete='0'";
		List<Question> questions = new ArrayList<Question>();
		questions = session.createQuery(query).list();
		return questions;
	}

	@Override
	public Question deleteById(String id) {
		Session session = sessionFactory.getCurrentSession();
		String query = "select q from Question q where id='"+id+"'";
		Question question = new Question();
		question = (Question) session.createQuery(query).getSingleResult();
		return question;
	}

	@Override
	public void delete(Question question) {
		Session session = sessionFactory.getCurrentSession();
		session.update(question);
		
	}

}
