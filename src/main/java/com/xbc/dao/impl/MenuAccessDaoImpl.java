package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.MenuAccessDao;
import com.xbc.model.MenuAccess;
import com.xbc.model.Role;

@Repository
public class MenuAccessDaoImpl implements MenuAccessDao {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void create(MenuAccess menuAccess) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(menuAccess);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MenuAccess> searchAll() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<MenuAccess> menuAccessList = new ArrayList<MenuAccess>();
		menuAccessList = session.createQuery("from MenuAccess").list();
		return menuAccessList;
	}

	@Override
	public List<MenuAccess> searchByRole(Role role) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select ma from MenuAccess ma where ma.roleMenuAccess='"+role.getId()+"'";
		List<MenuAccess> menuAccessList = new ArrayList<MenuAccess>();
		menuAccessList = session.createQuery(query).list();
		return menuAccessList;
	}

	@Override
	public MenuAccess searchById(String id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select ma from MenuAccess ma where ma.id='"+id+"'";
		MenuAccess menuAccess = new MenuAccess();
		menuAccess = (MenuAccess) session.createQuery(query).getSingleResult();
		return menuAccess;
	}

	@Override
	public void delete(MenuAccess menuAccess) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(menuAccess);
	}


}
