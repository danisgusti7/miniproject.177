package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.VersionDao;
import com.xbc.model.Version;

@Repository
public class VersionDaoImpl implements VersionDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public void create(Version versionQuestion) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(versionQuestion);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Version> getAllVersions() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Version> versions = new ArrayList<Version>();
		versions = session.createQuery("select v from Version v where isDelete='0'").list();
		return versions;
	}

	@Override
	public Version deleteById(String id) {
		Session session = this.sessionFactory.getCurrentSession();
		String query = "select ver from Version ver where id='"+id+"'";
		Version version = new Version();
		version = (Version) session.createQuery(query).getSingleResult();
		return version;
	}

	@Override
	public void delete(Version version) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(version);
		
	}

	@Override
	public String generateVersion() {
		Session session = sessionFactory.getCurrentSession();
		String query = "select ver from Version ver where ver.version = (select max(v.version) from Version v)";
		Version version = new Version();
		version = (Version) session.createQuery(query).getSingleResult();
		String newVersion = "";
		if(version == null) {
			newVersion = "1";
		}else {
			String oldVersion = Long.toString(version.getVersion());
			int oldVersionNumber = Integer.parseInt(oldVersion) + 1;
			newVersion = Integer.toString(oldVersionNumber);
		}
		
		return newVersion;
	}

}
