package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.management.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.BiodataDao;
import com.xbc.model.Biodata;

@Repository
public class BiodataDaoImpl implements BiodataDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void save(Biodata biodata) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(biodata);
	}

	
	@Override
	public List<Biodata> listAll() {
		// TODO Auto-generated method stub
		String hql = " select b from Biodata b where b.active=1 ";
		Session session = sessionFactory.getCurrentSession();
		List<Biodata> biodatas = new ArrayList<Biodata>();
		biodatas = session.createQuery(hql).list();
		return biodatas;
	}
	
	@Override
	public Biodata getBiodataById(Long id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Biodata idBiodata = session.get(Biodata.class, id);
		return idBiodata;
	}

	@Override
	public void update(Biodata biodata) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(biodata);
	}


	@Override
	public void delete(Biodata biodata) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(biodata);
	}
	
}
