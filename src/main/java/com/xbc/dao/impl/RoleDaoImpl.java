package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.RoleDao;
import com.xbc.model.Role;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> searchAll() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession(); 
		String query ="select r from Role r where r.active=1 order by r.code ";
		List<Role> roleList = new ArrayList<Role>();
		roleList = session.createQuery(query).list();
		return roleList;
	}

	@Override
	public void create(Role role) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(role);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> searchByName(String name) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select r from Role r where r.active=1 and lower(r.name) like lower('%"+name+"%')";
		List<Role> roleList = new ArrayList<Role>();
		roleList = session.createQuery(query).list();

		return roleList;
	}

	@Override
	public Role searchById(String id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select r from Role r where r.id='"+id+"'";
		Role role = new Role();
		role = (Role) session.createQuery(query).getSingleResult();
		return role;
	}

	@Override
	public void edit(Role role) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "update Role r set r.name =:name, r.description =:description, r.modifiedBy =:modifiedBy, r.modifiedOn =:modifiedOn"
				+ " where r.id = :id";
		Query query= session.createQuery(hql);
		query.setParameter("id", role.getId());
		query.setParameter("name", role.getName());
		query.setParameter("description", role.getDescription());
		query.setParameter("modifiedBy", role.getModifiedBy());
		query.setParameter("modifiedOn", role.getModifiedOn());
		query.executeUpdate();
	}

	@Override
	public void deactivate(Role role) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "update Role r set r.modifiedBy =:modifiedBy, r.modifiedOn =:modifiedOn, r.active =:active where r.id =:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", role.getId());
		query.setParameter("modifiedBy", role.getModifiedBy());
		query.setParameter("modifiedOn", role.getModifiedOn());
		query.setParameter("active", role.getActive());
		query.executeUpdate();
	}

	@Override
	public String generateCode() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select x from Role x where x.code = (select max(r.code) from Role r)";
		Role role = new Role();
		role = (Role) session.createQuery(query).getSingleResult();
		String newCode = "";
		if(role == null) {
			newCode ="RO0001";
		}else {
			String oldCode = role.getCode().split("O")[1];
			int oldCodeNumber = Integer.parseInt(oldCode) + 1;
			
			if(oldCodeNumber <10) {
				newCode = "RO000" + oldCodeNumber;
			}else if(oldCodeNumber <100) {
				newCode = "RO00" + oldCodeNumber;
			}else if(oldCodeNumber <1000) {
				newCode = "RO0" + oldCodeNumber;
			}else if(oldCodeNumber <10000) {
				newCode = "RO" + oldCodeNumber;
			}
			
		}
		return newCode;
	}



}
