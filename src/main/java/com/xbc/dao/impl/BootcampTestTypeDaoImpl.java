package com.xbc.dao.impl;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.BootcampTestTypeDao;
import com.xbc.model.BootcampTestType;

@Repository
public class BootcampTestTypeDaoImpl implements BootcampTestTypeDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<BootcampTestType> getAllTestType() {
		// TODO Auto-generated method stub
		String hql = "from BootcampTestType";
		Session session = sessionFactory.getCurrentSession();
		List<BootcampTestType> bootcampTestTypes = session.createQuery(hql).list();
		return bootcampTestTypes;
	}

}
