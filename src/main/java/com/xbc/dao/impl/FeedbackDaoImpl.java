package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.FeedbackDao;
import com.xbc.model.Feedback;
import com.xbc.model.Question;


@Repository
public class FeedbackDaoImpl implements FeedbackDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public void create(Feedback feedback) {
		Session session = sessionFactory.getCurrentSession();
		session.save(feedback);
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Question> getLastVersion() {
		Session session = sessionFactory.getCurrentSession();
		String query = "select q from Question q, VersionDetail v"+
				" where v.questionId = q.id and v.versionId = (select max(ver.versionId) from VersionDetail ver)";
		List<Question> questions = new ArrayList<Question>();
		questions = (List<Question>) session.createQuery(query).list();
		return questions;
	}
}
