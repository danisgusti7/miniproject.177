package com.xbc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xbc.dao.MenuDao;
import com.xbc.model.Menu;

@Repository
public class MenuDaoImpl implements MenuDao {
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> searchAll() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = session.createQuery("select m from Menu m where m.active=1").list();
		return menuList;
	}

	@Override
	public Menu searchById(String id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select m from Menu m where m.id='"+id+"'";
		Menu menu = new Menu();
		
		try {
			menu = (Menu) session.createQuery(query).getSingleResult();
		}catch(NoResultException nre){
			
		}
		if(menu == null) {
			return null;
		}else {
			return menu;
		}
	}


	@Override
	public void create(Menu menu) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(menu);
	}

	@Override
	public List<Menu> searchByTitle(String title) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select m from Menu m where m.active=1 and lower(m.title) like lower('%"+title+"%')";
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = session.createQuery(query).list();
		return menuList;
	}

	@Override
	public void edit(Menu menu) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(menu);
	}

	@Override
	public void deactivate(Menu menu) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(menu);
	}

	@Override
	public String generateCode() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "select y from Menu y where y.code = (select max(m.code) from Menu m)";
		Menu menu = new Menu();
		menu = (Menu) session.createQuery(query).getSingleResult();
		String newCode = "";
		if(menu == null) {
			newCode = "M0001";
		}else {
			String oldCode = menu.getCode().split("M")[1];
			
			int oldCodeNumber = Integer.parseInt(oldCode) +1;
			if(oldCodeNumber <10) {
				newCode = "M000" + oldCodeNumber;
			}else if(oldCodeNumber <100) {
				newCode = "M00" + oldCodeNumber;
			}else if(oldCodeNumber <1000) {
				newCode = "M0" + oldCodeNumber;
			}else if(oldCodeNumber <10000) {
				newCode = "M" + oldCodeNumber;
			}
		}
		return newCode;
	}

}
