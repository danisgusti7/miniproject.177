package com.xbc.dao;

import java.util.List;

import com.xbc.model.Version;

public interface VersionDao {

	void create(Version versionQuestion);

	List<Version> getAllVersions();

	Version deleteById(String id);

	void delete(Version version);

	String generateVersion();

}
