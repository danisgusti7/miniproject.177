package com.xbc.dao;

import java.util.List;

import com.xbc.model.Role;

public interface RoleDao {
	
	String generateCode();

	List<Role> searchAll();

	void create(Role role);

	List<Role> searchByName(String name);

	Role searchById(String id);

	void edit(Role role);

	void deactivate(Role role);

}
