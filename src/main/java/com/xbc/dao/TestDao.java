package com.xbc.dao;

import java.util.List;

import com.xbc.model.Test;

public interface TestDao {

	List<Test> getTest();

	Test getById(Long testFeedback);

}
