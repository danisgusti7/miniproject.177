package com.xbc.dao;

import java.util.List;

import com.xbc.model.Question;

public interface QuestionDao {

	public void create(Question question);
	public List<Question> getAllQuestions();
	public List<Question> searchByQuestion(String question);
	public Question deleteById(String id);
	public void delete(Question question);


}
