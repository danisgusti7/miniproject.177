package com.xbc.dao;

import java.util.List;

import com.xbc.model.Menu;

public interface MenuDao {

	List<Menu> searchAll();

	Menu searchById(String id);

	void create(Menu menu);

	List<Menu> searchByTitle(String title);

	void edit(Menu menu);

	void deactivate(Menu menu);
	
	String generateCode();

}
