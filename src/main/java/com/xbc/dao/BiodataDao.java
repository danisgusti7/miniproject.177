package com.xbc.dao;

import java.util.List;

import com.xbc.model.Biodata;

public interface BiodataDao {

	void save(Biodata biodata);

	List<Biodata> listAll();


	void update(Biodata biodata);

	Biodata getBiodataById(Long id);

	void delete(Biodata biodata);

}
