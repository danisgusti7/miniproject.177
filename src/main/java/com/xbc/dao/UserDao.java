package com.xbc.dao;

import java.util.List;

import com.xbc.model.User;

public interface UserDao {

	List<User> searchAll();

	void create(User user);

	List<User> searchByUsername(String username);
	
	User getByUsername(String username);

	User searchById(String id);

	void edit(User user);

	void deactivate(User user);




}
