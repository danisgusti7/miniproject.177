package com.xbc.dao;


import java.util.List;

import com.xbc.model.Feedback;
import com.xbc.model.Question;

public interface FeedbackDao {

	void create(Feedback feedback);

	List<Question> getLastVersion();
}
