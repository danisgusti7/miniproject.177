package com.xbc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Feedback;
import com.xbc.model.Question;
import com.xbc.model.Test;
import com.xbc.model.Version;
import com.xbc.service.FeedbackService;
import com.xbc.service.QuestionService;
import com.xbc.service.TestService;
import com.xbc.service.VersionService;

@Controller
public class FeedbackController {
	
	@Autowired
	FeedbackService feedbackService;
	
	@Autowired
	TestService testService;
	
	@Autowired
	VersionService versionService;
	
	@Autowired
	QuestionService questionService;
	
	@RequestMapping(value="feedback")
	public String home(Model model) {
		List<Test> tests = testService.getTest();
		List<Question> questionList = feedbackService.getLastVersion();
		List<Version> versions = versionService.getAllVersions();
		
		model.addAttribute("questionList", questionList);
		model.addAttribute("tests", tests);
		model.addAttribute("versions", versions);
		
		String jsp = "feedback/home";
		return jsp;
	}
	
	@RequestMapping(value="feedback/create")
	public String createFeedback(HttpServletRequest request) {
		String jsonFeedback = request.getParameter("jsonFeedback");
		Long testFeedback = Long.parseLong(request.getParameter("testType"));
		int isDelete = 0;
		
		Test test = new Test();
		test = testService.getById(testFeedback);
		
		Feedback feedback = new Feedback();
		feedback.setJsonFeedback(jsonFeedback);
		feedback.setTestFeedback(test);
		feedback.setCreatedOn(new Date());
		feedback.setDeletedOn(new Date());
		feedback.setIsDelete(isDelete);
		
		feedbackService.create(feedback);
		String jsp = "feedback/home";
		return jsp;
	}
}
