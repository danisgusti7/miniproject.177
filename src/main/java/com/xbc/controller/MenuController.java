package com.xbc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Menu;
import com.xbc.service.MenuService;

@Controller
public class MenuController extends BaseController {
	
	@Autowired
	MenuService menuService;
	
	@RequestMapping(value="menu")
	public String home() {
		String jsp = "menu/main";
		return jsp;
	}
	
	@RequestMapping(value="menu/list")
	public String list(Model model) {
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = menuService.searchAll();
		model.addAttribute("menuList", menuList);
		String jsp = "menu/list";
		return null;
	}
	
	@RequestMapping(value="menu/add")
	public String add(Model model) {
		List<Menu> menuList = menuService.searchAll();
		String newCode = menuService.generateCode();
		model.addAttribute("menuList", menuList);
		model.addAttribute("newCode", newCode);
		String jsp = "menu/add";
		return jsp;
	}
	
	@RequestMapping(value="menu/create")
	public String create(HttpServletRequest request) {
		String code = request.getParameter("code");
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String imageUrl = request.getParameter("imageUrl");
		Integer menuOrder = Integer.parseInt(request.getParameter("menuOrder"));
		String menuId = request.getParameter("menu.id");
		String menuUrl = request.getParameter("menuUrl");
	    Long createdBy = this.getUserId();
		int active = 1;
		
		Menu menuParent = new Menu();
		Menu menu = new Menu();

		if(!menuId.isEmpty()) {
			menuParent = menuService.searchById(menuId);
			menu.setMenuParent(menuParent);
		}
		
	    
		menu.setCode(code);
		menu.setTitle(title);
		menu.setDescription(description);
		menu.setImageUrl(imageUrl);
		menu.setMenuOrder(menuOrder);
		menu.setMenuUrl(menuUrl);
		menu.setCreatedBy(createdBy);
		menu.setCreatedOn(new Date());
		menu.setActive(active);
		
		menuService.create(menu);
	    
		String jsp = "menu/create";
		return null;
	}
	

	
	@RequestMapping(value="menu/search/title")
	public String searchTitle(HttpServletRequest request, Model model){
		String title = request.getParameter("titleSearch");
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = menuService.searchByTitle(title);
		model.addAttribute("menuList", menuList);
		
		String jsp = "menu/list";
		return jsp;
	}
	
	@RequestMapping(value="menu/edit")
	public String edit(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		Menu menu = new Menu();
		menu = menuService.searchById(id);
		List<Menu> menuList = menuService.searchAll();
		
		model.addAttribute("menu", menu);
		model.addAttribute("menuList", menuList);
		
		String jsp = "menu/edit";
		return jsp;
	}
	
	@RequestMapping(value="menu/edit/save")
	public String editSave(HttpServletRequest request) throws Exception{
		String code = request.getParameter("code");
		String id = request.getParameter("id");
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String imageUrl = request.getParameter("imageUrl");
		Integer menuOrder = Integer.parseInt(request.getParameter("menuOrder"));
		String menuId = request.getParameter("menu.id");
		String menuUrl = request.getParameter("menuUrl");
	    Long modifiedBy = this.getUserId();	
		
	    Menu menuParent = new Menu();
	    Menu menu = new Menu();
	    menuParent = menuService.searchById(menuId);
	    menu = menuService.searchById(id);
	    

		if(!menuId.isEmpty()) {
			menu.setMenuParent(menuParent);
		}else {
			menu.setMenuParent(null);
		}
		
	    
	    
		menu.setCode(code);
	    menu.setTitle(title);
	    menu.setDescription(description);
	    menu.setImageUrl(imageUrl);
	    menu.setMenuOrder(menuOrder);
	    menu.setMenuUrl(menuUrl);
	    menu.setModifiedBy(modifiedBy);
	    menu.setModifiedOn(new Date());
	    
	    menuService.edit(menu);
		
		String jsp = "menu/main";
		return null;
	}
	
	@RequestMapping(value="menu/deactivate")
	public String deactivate(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		Menu menu = new Menu();
		menu = menuService.searchById(id);
		model.addAttribute("menu", menu);
		
		String jsp = "menu/deactivate";
		return jsp;
	}
	
	@RequestMapping(value="menu/deactivate/save")
	public String deactivateSave(HttpServletRequest request) throws Exception {
		String id = request.getParameter("id");
		int deactivate = 0;
		Long modifiedBy = this.getUserId();
		
		Menu menu = new Menu();
		menu = menuService.searchById(id);
		
		menu.setModifiedBy(modifiedBy);
		menu.setModifiedOn(new Date());
		menu.setActive(deactivate);
		
		menuService.deactivate(menu);
		
		String jsp = "menu/main";
		return null;
	}
	
}
