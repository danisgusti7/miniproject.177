/**
 * 
 */
package com.xbc.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.xbc.service.UserService;

@Controller
public class BaseController {
	@Autowired
	UserService userService;
	
	public String getUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth!=null) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			return userDetail.getUsername();
		}
		return null;
	}
	
	public Long getUserId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth!=null) {
			User user = (User) auth.getPrincipal();
			return userService.getByUsername(user.getUsername()).getId();
		}
		return null;
	}
	
/*	public int getOutletId(HttpSession session){
		int outletId = 0;
		if(!session.getAttribute("outletId").toString().equals("")){
			outletId = Integer.parseInt(session.getAttribute("outletId").toString());
		}
		return outletId;
	}*/
}
