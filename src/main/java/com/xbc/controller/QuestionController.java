package com.xbc.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Question;
import com.xbc.service.QuestionService;

@Controller
public class QuestionController extends BaseController{
	
	@Autowired
	QuestionService questionService;
	
	@RequestMapping(value="question")
	public String home() {
		String jsp = "question/home";
		return jsp;
	}
	
	@RequestMapping(value="question/addQuestion")
	public String add() {
		String jsp = "question/addQuestion";
		return jsp;
	}
	
	@RequestMapping(value="question/list")
	public String list(Model model) {
		List<Question> questions = new ArrayList<Question>();
		questions = this.questionService.getAllQuestions();
		model.addAttribute("questions", questions);
		String jsp = "question/list";
		return jsp;
	}
	
	@RequestMapping(value = "question/create")
	public String createQuestion(HttpServletRequest request) {
		String questionContent = request.getParameter("questionContent");
		Long createdBy = this.getUserId();
		
		int isDelete = 0;
		
		Question question = new Question();
		question.setQuestion(questionContent);
		question.setCreatedBy(createdBy);
		question.setCreatedOn(new Date());
		//question.setModifiedOn(new Date());
		question.setIsDelete(isDelete);
		
		this.questionService.create(question);
		
		String jsp = "question/home";
		return jsp;
	}
	
	@RequestMapping(value="question/search/getQuestion")
	public String questionSearch(HttpServletRequest request, Model model) {
		String question = request.getParameter("searchQuestion");
		List<Question> questions = new ArrayList<Question>();
		questions = this.questionService.searchByQuestion(question);
		model.addAttribute("questions", questions);
		String jsp = "question/list";
		return jsp;
	}
	
	@RequestMapping(value="question/delete")
	public String deleteQuestion(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		
		Question question = new Question();
		question = this.questionService.deleteById(id);
		
		model.addAttribute("question", question);
		
		String jsp = "question/delete";
		return jsp;
	}
	
	@RequestMapping(value="question/delete/save")
	public String saveData(HttpServletRequest request) throws Exception {
		String id = request.getParameter("id");
		Long deletedBy = this.getUserId();
		int isDelete = 1;
		Question question = new Question();
		question = this.questionService.deleteById(id);
		question.setDeletedBy(deletedBy);
		question.setIsDelete(isDelete);
		question.setDeletedOn(new Date());
		this.questionService.delete(question);
		
		String jsp = "question/home";
		return jsp;
	}
	
}
