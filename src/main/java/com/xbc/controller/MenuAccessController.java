package com.xbc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Menu;
import com.xbc.model.MenuAccess;
import com.xbc.model.Role;
import com.xbc.service.MenuAccessService;
import com.xbc.service.MenuService;
import com.xbc.service.RoleService;

@Controller
public class MenuAccessController extends BaseController {

	@Autowired
	MenuAccessService menuAccessService;
	@Autowired
	RoleService roleService;
	@Autowired
	MenuService menuService;
	
	@RequestMapping(value="menuAccess")
	public String home(Model model) {
		List<Role> roleList = roleService.searchAll();
		model.addAttribute("roleList", roleList);
		String jsp = "menuAccess/main";
		return jsp;
	}
	
	@RequestMapping(value="menuAccess/list")
	public String list(Model model) {
		List<MenuAccess> menuAccessList = new ArrayList<MenuAccess>();
		menuAccessList = menuAccessService.searchAll();
		model.addAttribute("menuAccessList", menuAccessList);
		
		String jsp = "menuAccess/list";
		return null;
	}
	
	
	@RequestMapping(value="menuAccess/add")
	public String add(Model model) {
		List<Role> roleList = roleService.searchAll();
		List<Menu> menuList = menuService.searchAll();
		model.addAttribute("roleList", roleList);
		model.addAttribute("menuList", menuList);
		
		String jsp = "menuAccess/add";
		return jsp;
	}
	
	@RequestMapping(value="menuAccess/create")
	public String create(@ModelAttribute MenuAccess menu) {

	    Long createdBy = this.getUserId();
		

		menu.setCreatedBy(createdBy);
		menu.setCreatedOn(new Date());
		
		menuAccessService.create(menu);
	    
	    
		String jsp = "menuAccess/main";
		return null;
	}
	
	@RequestMapping(value="menuAccess/search/role")
	public String searchRole(HttpServletRequest request, Model model) {
		String roleId = request.getParameter("roleSearch");
		
		Role role = new Role();
		role = roleService.searchById(roleId);
		
		List<MenuAccess> menuAccessList = new ArrayList<MenuAccess>();
		menuAccessList = menuAccessService.searchByRole(role);
		
		model.addAttribute("role", role);
		model.addAttribute("menuAccessList", menuAccessList);
		
		String jsp = "menuAccess/list";
		return jsp;
	}
	
	@RequestMapping(value="menuAccess/delete")
	public String delete(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		MenuAccess menuAccess = new MenuAccess();
		menuAccess = menuAccessService.searchById(id);
		model.addAttribute("menuAccess", menuAccess);
		
		String jsp = "menuAccess/delete";
		return jsp;
	}
	
	@RequestMapping(value="menuAccess/delete/save")
	public String deleteSave(HttpServletRequest request) throws Exception {
		String id = request.getParameter("id");
		
		MenuAccess menuAccess = new MenuAccess();
		menuAccess = menuAccessService.searchById(id);
		
		menuAccessService.delete(menuAccess);
		
		return null;
	}

	
}
