package com.xbc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Role;
import com.xbc.model.User;
import com.xbc.service.RoleService;
import com.xbc.service.UserService;

@Controller
public class UserController extends BaseController {

	@Autowired
	UserService userService;
	
	@Autowired
	RoleService roleService;
	
	@RequestMapping(value="user")
	public String home() {
		String jsp = "user/main";
		return jsp;
	}
	
	@RequestMapping(value="user/list")
	public String list(Model model) {
		List<User> userList = new ArrayList<User>();
		userList = userService.searchAll();
		model.addAttribute("userList", userList);
		String jsp = "user/list";
		return null;
	}
	
	@RequestMapping(value="user/add")
	public String add(Model model) {
		List<Role> roleList = roleService.searchAll();
		model.addAttribute("roleList", roleList);
		String jsp = "user/add";
		return jsp;
	}
	
/*	@RequestMapping(value = "user/create")
	public String create(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String roleId = request.getParameter("role.id");
		int mobileFlag = 0;
	    Long createdBy = this.getUserId();
	    int active = 1;
	    
	    Role role =  new Role();
	    role = roleService.searchById(roleId);
	    
	    User user = new User();
	    user.setUsername(username);
	    user.setPassword(password);
	    user.setRoleUser(role);
	    user.setMobileFlag(mobileFlag);
	    user.setCreatedBy(createdBy);
	    user.setCreatedOn(new Date());
	    user.setActive(active);
	    
	    userService.create(user);
	    
	    String jsp ="user/main";
		return null;
	}*/
	
	@RequestMapping(value = "user/create")
	public String create(@ModelAttribute User user) {
		int mobileFlag = 0;
		Long mobileToken = null;
	    Long createdBy = this.getUserId();
	    int active = 1;
	    
	    

	    user.setMobileFlag(mobileFlag);
	    user.setMobileToken(mobileToken);
	    user.setCreatedBy(createdBy);
	    user.setCreatedOn(new Date());
	    user.setActive(active);
	    
	    userService.create(user);
	    
	    String jsp ="user/main";
		return null;
	}
	
	@RequestMapping(value="user/search/username")
	public String searchUsername(HttpServletRequest request, Model model) {
		String username = request.getParameter("usernameSearch");
		List<User> userList = new ArrayList<User>();
		userList = userService.searchByUsername(username);
		model.addAttribute("userList", userList);
		
		String jsp ="user/list";
		return jsp;
	}
	
	@RequestMapping(value="user/edit")
	public String edit(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		User user = new User();
		user = userService.searchById(id);
		List<Role> roleList = roleService.searchAll();
		
		model.addAttribute("user", user);
		model.addAttribute("roleList", roleList);
		
		String jsp = "user/edit";
		return jsp;
	}
	
	
/*	@RequestMapping(value="user/edit/save")
	public String editSave(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String roleId = request.getParameter("role.id");
	    Long modifiedBy = this.getUserId();		
		
	    Role role = new Role();
	    role = roleService.searchById(roleId);
	    
	    User user = new User();
	    user = userService.searchById(id);
	    
	    user.setUsername(username);
	    user.setPassword(password);
	    user.setRoleUser(role);
	    user.setModifiedBy(modifiedBy);
	    user.setModifiedOn(new Date());
	    
	    userService.edit(user);
	    
		
		String jsp = "user/main";
		return null;
	}*/
	
	@RequestMapping(value="user/edit/save")
	public String editSave(@ModelAttribute User user){
		User oldUser = userService.searchById(Long.toString(user.getId()));
		
		Long modifiedBy = this.getUserId();		
		
		user.setCreatedBy(oldUser.getCreatedBy());
		user.setCreatedOn(oldUser.getCreatedOn());
		user.setActive(oldUser.getActive());
	    user.setModifiedBy(modifiedBy);
	    user.setModifiedOn(new Date());
	    
	    userService.edit(user);
	    
		
		String jsp = "user/main";
		return null;
	}
	
	@RequestMapping(value="user/deactivate")
	public String deactivate(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		User user = new User();
		user = userService.searchById(id);
		model.addAttribute("user", user);
		
		String jsp = "user/deactivate";
		return jsp;
	}
	
	@RequestMapping(value="user/deactivate/save")
	public String deactivateSave(@ModelAttribute User user) throws Exception {
		User oldUser = userService.searchById(Long.toString(user.getId()));
		
		Long modifiedBy =  this.getUserId();
		int deactive = 0;
		

		user.setUsername(oldUser.getUsername());
		user.setPassword(oldUser.getPassword());
		user.setMobileFlag(oldUser.getMobileFlag());
		user.setMobileToken(oldUser.getMobileToken());
		user.setCreatedBy(oldUser.getCreatedBy());
		user.setCreatedOn(oldUser.getCreatedOn());
		user.setModifiedBy(modifiedBy);
		user.setModifiedOn(new Date());
		user.setActive(deactive);
		
		userService.deactivate(user);
		
		String jsp = "user/main";
		return null;
	}
	
}
