package com.xbc.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xbc.model.Biodata;
import com.xbc.model.BootcampTestType;
import com.xbc.service.BiodataService;
import com.xbc.service.BootcampTestTypeService;

@Controller
@RequestMapping(value = "/biodata")
public class BiodataContoller {

	@Autowired
	BiodataService biodataService;
	
	@Autowired
	BootcampTestTypeService bootcampTestTypeService;
	
	@RequestMapping
	public String home() {
		return "/biodata/home";
	}

	@RequestMapping(value = "/save")
	public String save(HttpServletRequest req, Model model) {

		try {
			String biodataName = req.getParameter("biodataName");
			String lastEducation = req.getParameter("lastEducation");
			String educationalLevel = req.getParameter("educationLevel");
			String graduationYear = req.getParameter("graduationYear");
			String majors = req.getParameter("majors");
			String gpa = req.getParameter("gpa");
			Long idUser = (long) 1;
			int active = 1;

			Biodata biodata = new Biodata();
			biodata.setName(biodataName);
			biodata.setLastEducation(lastEducation);
			biodata.setEducationalLevel(educationalLevel);
			biodata.setGraduationYear(graduationYear);
			biodata.setMajors(majors);
			biodata.setGpa(gpa);
			biodata.setCreatedBy(idUser);
			biodata.setCreatedOn(new Date());
			biodata.setActive(active);
			biodataService.save(biodata);

			model.addAttribute("success", true);
		} catch (Exception e) {
			model.addAttribute("success", false);
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/list")
	public String list(Model model) {

		try {
			List<Biodata> biodatas = biodataService.listAll();
			model.addAttribute("listBiodata", biodatas);
			model.addAttribute("success", Boolean.TRUE);
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("success", Boolean.FALSE);
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(HttpServletRequest req, Model model) {
		
		try {
		
		Long id = Long.parseLong(req.getParameter("id"));
		Biodata biodata = biodataService.getBiodataById(id);
		
		String biodataName = req.getParameter("biodataName");
		String lastEducation = req.getParameter("lastEducation");
		String educationLevel = req.getParameter("educationLevel");
		String graduationYear = req.getParameter("graduationYear");
		String majors		= req.getParameter("majors");
		String gpa			= req.getParameter("gpa");
		
		List<BootcampTestType> bootcampTestTypes = bootcampTestTypeService.getAllTestType();
		model.addAttribute("testType", bootcampTestTypes);
		
		biodata.setName(biodataName);
		biodata.setLastEducation(lastEducation);
		biodata.setEducationalLevel(educationLevel);
		biodata.setGraduationYear(graduationYear);
		biodata.setMajors(majors);
		biodata.setGpa(gpa);
		biodata.setModifiedOn(new Date());
		biodataService.update(biodata);
		
		model.addAttribute("success", true);
		
		}catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("success", false);
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String delete(HttpServletRequest req, Model model) {
		
		try {
		Long id = Long.parseLong(req.getParameter("id"));
		Biodata biodata = biodataService.getBiodataById(id);
		
		biodata.setActive(0);
		biodataService.delete(biodata);
		
		model.addAttribute("success", true);
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("success", false);
			e.printStackTrace();
		}
		return null;
	}
}
