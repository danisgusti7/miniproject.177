package com.xbc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


import com.xbc.model.Role;
import com.xbc.service.RoleService;

@Controller
public class RoleController extends BaseController {
	
	@Autowired
	RoleService roleService;
	
	@RequestMapping(value="role")
	public String home() {
		String jsp = "role/main";
		return jsp;
	}
	
	@RequestMapping(value="role/list")
	public String list(Model model) {
		List<Role> roleList = new ArrayList<Role>();
		roleList = roleService.searchAll();
	
		model.addAttribute("roleList", roleList);
		/*model.addAttribute("success", Boolean.TRUE);*/
		String jsp = "role/list";
		return null;
	}
	
	
	@RequestMapping(value="role/add")
	public String add(Model model) {
		String newCode = roleService.generateCode();
		model.addAttribute("newCode", newCode);
		String jsp = "role/add";
		return jsp;
	}
	
	@RequestMapping(value = "role/create")
	//Model Attribute = mencocokan inputan user dengan model
	public String create(@ModelAttribute Role role) {
	
		Long createdBy = this.getUserId();
	    int active = 1;

		role.setCreatedBy(createdBy);
		role.setCreatedOn(new Date());
		role.setActive(active);
		
		roleService.create(role);
		
		String jsp = "role/main";
		return null;
	}
	
	@RequestMapping(value="role/search/name") //URL di web
	public String searchName(HttpServletRequest request, Model model) {
		String name = request.getParameter("nameSearch"); //ambil dari ajax
		List<Role> roleList = new ArrayList<Role>();
		roleList = roleService.searchByName(name);
		model.addAttribute("roleList", roleList);

		String jsp = "role/list";
		return jsp;
	}
	
	@RequestMapping(value="role/edit")
	public String edit(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		Role role = new Role();
		role = roleService.searchById(id);
		model.addAttribute("role", role);
		
		String jsp = "role/edit";
		return jsp;
	}
	
/*	@RequestMapping(value="role/edit/save")
	public String editSave(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String description = request.getParameter("description");
	    Long modifiedBy = this.getUserId();
		
		Role role = new Role();
		role = roleService.searchById(id);
		
		role.setName(name);
		role.setDescription(description);
		role.setModifiedBy(modifiedBy);
		role.setModifiedOn(new Date());
		
		roleService.edit(role);
		
		String jsp = "role/main";
		return null;
	}*/
	
	@RequestMapping(value="role/edit/save")
	public String editSave(@ModelAttribute Role role){
		Role oldRole = roleService.searchById(Long.toString(role.getId()));
	    Long modifiedBy = this.getUserId();
	    
	    role.setCreatedBy(oldRole.getCreatedBy());
	    role.setCreatedOn(oldRole.getCreatedOn());
	    role.setActive(oldRole.getActive());
/*	    role.setUsers(roleLama.getUsers());
	    role.setMenuAccesses(roleLama.getMenuAccesses());*/
		role.setModifiedBy(modifiedBy);
		role.setModifiedOn(new Date());
		
		roleService.edit(role);
		
		String jsp = "role/main";
		return null;
	}
	
	@RequestMapping(value="role/deactivate")
	public String deactivate(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		Role role = new Role();
		role = roleService.searchById(id);
		model.addAttribute("role",role);
		
		String jsp = "role/deactivate";
		return jsp;
	}
	
	@RequestMapping(value="role/deactivate/status")
	public String deactivateSave(@ModelAttribute Role role) throws Exception{
		Role oldRole = roleService.searchById(Long.toString(role.getId()));
		int deactive = 0;
		Long modifiedBy = this.getUserId();
		
		role.setCode(oldRole.getCode());
		role.setName(oldRole.getName());
		role.setDescription(oldRole.getDescription());
		role.setCreatedBy(oldRole.getCreatedBy());
		role.setCreatedOn(oldRole.getCreatedOn());
		role.setModifiedBy(modifiedBy);
		role.setModifiedOn(new Date());
		role.setActive(deactive);
		
		roleService.deactivate(role);
		
		String jsp = "role/main";
		return null;
	}
	

}
