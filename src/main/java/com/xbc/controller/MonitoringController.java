package com.xbc.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Monitoring;
import com.xbc.service.MonitoringService;

@Controller
@RequestMapping(value="/monitoring")
public class MonitoringController {

	@Autowired
	MonitoringService monitoringService;
	
	@RequestMapping
	public String home() {
		return "monitoring/home";
	}
	
	@RequestMapping(value="/save")
	public String save(HttpServletRequest req) {
		
		String idleName = req.getParameter("idleName");
		String idleDate = req.getParameter("idleDate");
		String idleLastProject = req.getParameter("idleLastProject");
		String idleReason = req.getParameter("idleReason");
		
		Monitoring monitoring = new Monitoring();
		
		
		return "/monitoring/home";
	}
}
