package com.xbc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.model.Question;
import com.xbc.model.Version;
import com.xbc.service.QuestionService;
import com.xbc.service.VersionService;

@Controller
public class VersionController extends BaseController{

	@Autowired
	VersionService versionService;
	
	@Autowired
	QuestionService questionService;
	
	@RequestMapping(value="version")
	public String home() {
		String jsp = "version/home";
		return jsp;
	}
	
	@RequestMapping(value="version/create")
	public String createVersion(@RequestBody Version versionQuestion) {
		Long createdBy = this.getUserId();
		int isDelete = 0;
		
		versionQuestion.setCreatedBy(createdBy);
		versionQuestion.setCreatedOn(new Date());
		versionQuestion.setIsDelete(isDelete);
		
		this.versionService.create(versionQuestion);
		
		String jsp = "version/home";
		return jsp;
	}
	
	@RequestMapping(value="version/list")
	public String listVersion(Model model) {
		List<Version> versions = new ArrayList<Version>();
		List<Question> questions = new ArrayList<Question>();
		versions = this.versionService.getAllVersions();
		questions = this.questionService.getAllQuestions();
		model.addAttribute("versions", versions);
		model.addAttribute("questions", questions);
		String jsp = "version/list";
		return jsp;
	}
	
	@RequestMapping(value="version/addVersion")
	public String addVer(Model model) {
		String newVersion = versionService.generateVersion();
		model.addAttribute("newVersion", newVersion);
		List<Question> questions = new ArrayList<Question>();
		questions = this.questionService.getAllQuestions();
		model.addAttribute("questions", questions);
		String jsp = "version/addVersion";
		return jsp;
	}
	
	@RequestMapping(value="version/addSelection")
	public String addSelection(Model model) {
		List<Question> questions = new ArrayList<Question>();
		questions = this.questionService.getAllQuestions();
		model.addAttribute("questions", questions);
		String jsp = "version/addSelection";
		return jsp;
	}
	
	@RequestMapping(value="version/delete")
	public String deleteVersion(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		Version version = new Version();
		version = this.versionService.deleteById(id);
		model.addAttribute("version", version);
		String jsp = "version/delete";
		return jsp;
	}
	
	@RequestMapping(value="version/delete/saveVersion")
	public String saveData(HttpServletRequest request) throws Exception {
		String id = request.getParameter("id");
		Long deletedBy = this.getUserId();
		int isDelete = 1;
		Version version = new Version();
		version = this.versionService.deleteById(id);
		version.setDeletedBy(deletedBy);
		version.setDeletedOn(new Date());
		version.setIsDelete(isDelete);
		this.versionService.delete(version);
		
		String jsp = "version/home";
		return jsp;
	}
}
