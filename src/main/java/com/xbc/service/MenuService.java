package com.xbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xbc.dao.MenuDao;
import com.xbc.model.Menu;
import com.xbc.model.MenuAccess;


@Service
@Transactional
public class MenuService{
	@Autowired
	MenuDao menuDao;
	
	public List<Menu> searchAll() {
		// TODO Auto-generated method stub
		return menuDao.searchAll();
	}

	public Menu searchById(String id) {
		// TODO Auto-generated method stub
		return menuDao.searchById(id);
	}

	public void create(Menu menu) {
		// TODO Auto-generated method stub
		menuDao.create(menu);
	}

	public List<Menu> searchByTitle(String title) {
		// TODO Auto-generated method stub
		return menuDao.searchByTitle(title);
	}

	public void edit(Menu menu) {
		// TODO Auto-generated method stub
		menuDao.edit(menu);
	}

	public void deactivate(Menu menu) {
		// TODO Auto-generated method stub
		menuDao.deactivate(menu);
	}
	
	public String generateCode() {
		return menuDao.generateCode();
	}


}
