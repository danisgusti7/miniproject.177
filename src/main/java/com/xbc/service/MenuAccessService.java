package com.xbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xbc.dao.MenuAccessDao;
import com.xbc.model.MenuAccess;
import com.xbc.model.Role;


@Service
@Transactional
public class MenuAccessService{
	@Autowired
	MenuAccessDao menuAccessDao;

	public void create(MenuAccess menuAccess) {
		// TODO Auto-generated method stub
		menuAccessDao.create(menuAccess);
	}

	public List<MenuAccess> searchAll() {
		// TODO Auto-generated method stub
		return menuAccessDao.searchAll();
	}


	public List<MenuAccess> searchByRole(Role role) {
		// TODO Auto-generated method stub
		return menuAccessDao.searchByRole(role);
	}

	public MenuAccess searchById(String id) {
		// TODO Auto-generated method stub
		return menuAccessDao.searchById(id);
	}

	public void delete(MenuAccess menuAccess) {
		// TODO Auto-generated method stub
		menuAccessDao.delete(menuAccess);
	}



}
