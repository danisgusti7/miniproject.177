package com.xbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xbc.dao.RoleDao;
import com.xbc.model.Role;


@Service
@Transactional
public class RoleService{
	
	@Autowired
	RoleDao roleDao;
	
	public List<Role> searchAll() {
		// TODO Auto-generated method stub
		return this.roleDao.searchAll();
	}

	public void create(Role role) {
		// TODO Auto-generated method stub
		this.roleDao.create(role);
	}

	public List<Role> searchByName(String name) {
		// TODO Auto-generated method stub
		return roleDao.searchByName(name);
	}

	public Role searchById(String id) {
		// TODO Auto-generated method stub
		return roleDao.searchById(id);
	}

	public void edit(Role role) {
		// TODO Auto-generated method stub
		roleDao.edit(role);
	}

	public void deactivate(Role role) {
		// TODO Auto-generated method stub
		roleDao.deactivate(role);
	}
	
	public String generateCode() {
		return roleDao.generateCode();
	}


}
