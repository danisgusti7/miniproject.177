package com.xbc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xbc.dao.QuestionDao;
import com.xbc.model.Question;


@Service
@Transactional
public class QuestionService {
	
	@Autowired
	QuestionDao questionDao;
	
	public void create(Question question) {
		this.questionDao.create(question);
	}

	public List<Question> getAllQuestions() {
		return this.questionDao.getAllQuestions();
	}

	public List<Question> searchByQuestion(String question) {
		return this.questionDao.searchByQuestion(question);
	}

	public Question deleteById(String id) {
		return this.questionDao.deleteById(id);
	}

	public void delete(Question question) {
		this.questionDao.delete(question);
		
	}

}
