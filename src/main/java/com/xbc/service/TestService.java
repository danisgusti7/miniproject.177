package com.xbc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xbc.dao.TestDao;
import com.xbc.model.Test;

@Service
@Transactional
public class TestService {

	@Autowired
	TestDao testDao;
	
	public List<Test> getTest() {
		return testDao.getTest();
	}

	public Test getById(Long testFeedback) {
		return testDao.getById(testFeedback);
	}

}
