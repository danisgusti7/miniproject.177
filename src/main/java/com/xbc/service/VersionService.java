package com.xbc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xbc.dao.VersionDao;
import com.xbc.dao.VersionDetailDao;
import com.xbc.model.Version;
import com.xbc.model.VersionDetail;

@Service
@Transactional
public class VersionService {

	@Autowired
	VersionDao versionDao;
	
	@Autowired
	VersionDetailDao versionDetailDao;
	
	public List<Version> getAllVersions() {
		return this.versionDao.getAllVersions();
	}

	public void create(Version versionQuestion) {
		List<VersionDetail> versionDetails = versionQuestion.getVersionDetails();
		this.versionDao.create(versionQuestion);
		
		//java foreach
		for (VersionDetail versionDetail: versionDetails) {
			versionDetail.setVersionId(versionQuestion);
			versionDetailDao.create(versionDetail);
		}
		
	}

	public Version deleteById(String id) {
		return this.versionDao.deleteById(id);
	}

	public void delete(Version version) {
		this.versionDao.delete(version);
		
	}

	public String generateVersion() {
		return versionDao.generateVersion();
	}

}
