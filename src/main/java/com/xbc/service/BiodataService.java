package com.xbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xbc.dao.BiodataDao;
import com.xbc.model.Biodata;

@Service
@Transactional
public class BiodataService {
	
	@Autowired
	BiodataDao biodataDao;

	public void save(Biodata biodata) {
		// TODO Auto-generated method stub
		biodataDao.save(biodata);
	}

	public List<Biodata> listAll() {
		// TODO Auto-generated method stub
		return biodataDao.listAll();
	}

	public void update(Biodata biodata) {
		// TODO Auto-generated method stub
		biodataDao.update(biodata);
	}

	public Biodata getBiodataById(Long id) {
		// TODO Auto-generated method stub
		return biodataDao.getBiodataById(id);
	}

	public void delete(Biodata biodata) {
		// TODO Auto-generated method stub
		biodataDao.delete(biodata);
	}
}
