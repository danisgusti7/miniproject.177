package com.xbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xbc.dao.UserDao;
import com.xbc.model.User;

@Service
@Transactional
public class UserService{
	
	@Autowired
	UserDao userDao;
	
	public List<User> searchAll() {
		// TODO Auto-generated method stub
		return userDao.searchAll();
	}

	public void create(User user) {
		// TODO Auto-generated method stub
		userDao.create(user);
	}

	public List<User> searchByUsername(String username) {
		// TODO Auto-generated method stub
		return userDao.searchByUsername(username);
	}
	
	public User getByUsername(String username) {
		return userDao.getByUsername(username);
	}

	public User searchById(String id) {
		// TODO Auto-generated method stub
		return userDao.searchById(id);
	}

	public void edit(User user) {
		// TODO Auto-generated method stub
		userDao.edit(user);
	}

	public void deactivate(User user) {
		// TODO Auto-generated method stub
		userDao.deactivate(user);
	}






}
