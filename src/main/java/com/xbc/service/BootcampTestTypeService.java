package com.xbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xbc.dao.BootcampTestTypeDao;
import com.xbc.model.BootcampTestType;

@Service
@Transactional
public class BootcampTestTypeService {

	@Autowired
	BootcampTestTypeDao bootcampTestTypeDao;

	public List<BootcampTestType> getAllTestType() {
		// TODO Auto-generated method stub
		return bootcampTestTypeDao.getAllTestType();
	}
}
