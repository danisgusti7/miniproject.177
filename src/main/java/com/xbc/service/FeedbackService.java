package com.xbc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xbc.dao.FeedbackDao;
import com.xbc.model.Feedback;
import com.xbc.model.Question;

@Service
@Transactional
public class FeedbackService {

	@Autowired
	FeedbackDao feedbackDao;
	
	public void create(Feedback feedback) {
		feedbackDao.create(feedback);
		
	}

	public List<Question> getLastVersion() {
		return feedbackDao.getLastVersion();
	}

}
