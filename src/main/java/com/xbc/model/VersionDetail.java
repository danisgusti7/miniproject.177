package com.xbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_VERSION_DETAIL")
public class VersionDetail {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="QUESTION_ID")
	private Question questionId;
	
	@ManyToOne
	@JoinColumn(name="VERSION_ID")
	private Version versionId;
	

	@Column(name="CREATED_BY")
	private long createdBy;
	@Column(name="CREATED_ON")
	@Temporal(TemporalType.DATE)
	private Date createdOn;

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Version getVersionId() {
		return versionId;
	}


	public void setVersionId(Version versionId) {
		this.versionId = versionId;
	}


	public Question getQuestionId() {
		return questionId;
	}


	public void setQuestionId(Question questionId) {
		this.questionId = questionId;
	}
	
}
