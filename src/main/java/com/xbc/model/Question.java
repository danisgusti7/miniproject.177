package com.xbc.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_QUESTION")
public class Question {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String question;
	@Column(name="CREATED_BY")
	private long createdBy;
	@Column(name="CREATED_ON")
	@Temporal(TemporalType.DATE)
	private Date createdOn;
	@Column(name="MODIFIED_BY")
	private long modifiedBy;
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON")
	private Date modifiedOn;
	@Column(name="DELETED_BY")
	private long deletedBy;
	@Temporal(TemporalType.DATE)
	@Column(name="DELETED_ON")
	private Date deletedOn;
	@Column(name="IS_DELETE")
	private int isDelete;
	
	//mappedby = pake object di many to one
	@OneToMany(mappedBy="questionId")
	private List<VersionDetail> versionDetails;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public List<VersionDetail> getVersionDetails() {
		return versionDetails;
	}

	public void setVersionDetails(List<VersionDetail> versionDetails) {
		this.versionDetails = versionDetails;
	}
	
}
