package com.xbc.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="T_MENU")
public class Menu {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String code;
	private String title;
	private String description;
	@Column(name="IMAGE_URL")
	private String imageUrl;
	@Column(name="MENU_ORDER")
	private Integer menuOrder;
	
	
/*	@Column(name="MENU_PARENT")
	private Long menuParent;*/
	
	@ManyToOne
	@JoinColumn(name="MENU_PARENT")
	private Menu menuParent;
	
	@OneToMany(mappedBy="menuParent")
	private List<Menu> menuMenu;
	
	@Column(name="MENU_URL")
	private String menuUrl;
	@Column(name="CREATED_BY")
	private Long createdBy;
	@Column(name="CREATED_ON")
	private Date createdOn;
	@Column(name="MODIFIED_BY")
	private Long modifiedBy;
	@Column(name="MODIFIED_ON")
	private Date modifiedOn;
	private int active;
	
	@OneToMany(mappedBy="menuMenuAccess")
	private List<MenuAccess> menuAccesses;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Integer getMenuOrder() {
		return menuOrder;
	}
	public void setMenuOrder(Integer menuOrder) {
		this.menuOrder = menuOrder;
	}
/*	public Long getMenuParent() {
		return menuParent;
	}
	public void setMenuParent(Long menuParent) {
		this.menuParent = menuParent;
	}*/

	public Menu getMenuParent() {
		return menuParent;
	}
	public void setMenuParent(Menu menuParent) {
		this.menuParent = menuParent;
	}
	
	public List<Menu> getMenumenu() {
		return menuMenu;
	}
	public void setMenumenu(List<Menu> menuMenu) {
		this.menuMenu = menuMenu;
	}
	
	
	public String getMenuUrl() {
		return menuUrl;
	}
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public List<MenuAccess> getMenuAccesses() {
		return menuAccesses;
	}
	public void setMenuAccesses(List<MenuAccess> menuAccesses) {
		this.menuAccesses = menuAccesses;
	}
	
	
}
