package com.xbc.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_FEEDBACK")
public class Feedback {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="TEST_ID")
	private Test testFeedback;
	
	@ManyToOne
	@JoinColumn(name="VERSION_ID")
	private Version versionFeedback;
	
	private String jsonFeedback;
	private long createdBy;
	@Temporal(TemporalType.DATE)
	private Date createdOn;
	private long deletedBy;
	@Temporal(TemporalType.DATE)
	private Date deletedOn;
	private int isDelete;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Test getTestFeedback() {
		return testFeedback;
	}
	public void setTestFeedback(Test testFeedback) {
		this.testFeedback = testFeedback;
	}
	public Version getVersionFeedback() {
		return versionFeedback;
	}
	public void setVersionFeedback(Version versionFeedback) {
		this.versionFeedback = versionFeedback;
	}
	public String getJsonFeedback() {
		return jsonFeedback;
	}
	public void setJsonFeedback(String jsonFeedback) {
		this.jsonFeedback = jsonFeedback;
	}
	public long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public long getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
}
