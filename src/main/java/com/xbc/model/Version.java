package com.xbc.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_VERSION")
public class Version {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Long version;
	@Column(name="CREATED_BY")
	private Long createdBy;
	@Column(name="CREATED_ON")
	@Temporal(TemporalType.DATE)
	private Date createdOn;
	@Column(name="MODIFIED_BY")
	private Long modifiedBy;
	@Column(name="MODIFIED_ON")
	@Temporal(TemporalType.DATE)
	private Date modifiedOn;
	@Column(name="DELETED_BY")
	private Long deletedBy;
	@Column(name="DELETED_ON")
	@Temporal(TemporalType.DATE)
	private Date deletedOn;
	@Column(name="IS_DELETE")
	private int isDelete;
	
	@OneToMany(mappedBy="versionId")
	private List<VersionDetail> versionDetails;
	
	@OneToMany(mappedBy="versionFeedback")
	private List<Feedback> feedbacks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public List<VersionDetail> getVersionDetails() {
		return versionDetails;
	}

	public void setVersionDetails(List<VersionDetail> versionDetails) {
		this.versionDetails = versionDetails;
	}

	public List<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(List<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}

	
}
