package com.xbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_USER")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String username;
	private String password;
	
	@ManyToOne
	@JoinColumn(name= "ROLE_ID")
	private Role roleUser;
	
	/*@Column(name="ROLE_ID")
	private Long roleId;*/
	
	@Column(name="MOBILE_FLAG")
	private int mobileFlag;
	@Column(name="MOBILE_TOKEN")
	private Long mobileToken;
	@Column(name="CREATED_BY")
	private Long createdBy;
	@Column(name="CREATED_ON")
	private Date createdOn;
	@Column(name="MODIFIED_BY")
	private Long modifiedBy;
	@Column(name="MODIFIED_ON")
	private Date modifiedOn;
	private int active;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public Role getRoleUser() {
		return roleUser;
	}
	public void setRoleUser(Role roleUser) {
		this.roleUser = roleUser;
	}
	public int getMobileFlag() {
		return mobileFlag;
	}
	public void setMobileFlag(int mobileFlag) {
		this.mobileFlag = mobileFlag;
	}
	public Long getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(Long mobileToken) {
		this.mobileToken = mobileToken;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
	
}
