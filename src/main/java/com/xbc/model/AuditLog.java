package com.xbc.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AUDIT_LOG")
public class AuditLog {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String type;
	private String jsonInsert;
	private String jsonBefore;
	private String jsonAfter;
	private String jsonDelete;
	private Long createdBy;
	private Date createdOn;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getJsonInsert() {
		return jsonInsert;
	}
	public void setJsonInsert(String jsonInsert) {
		this.jsonInsert = jsonInsert;
	}
	public String getJsonBefore() {
		return jsonBefore;
	}
	public void setJsonBefore(String jsonBefore) {
		this.jsonBefore = jsonBefore;
	}
	public String getJsonAfter() {
		return jsonAfter;
	}
	public void setJsonAfter(String jsonAfter) {
		this.jsonAfter = jsonAfter;
	}
	public String getJsonDelete() {
		return jsonDelete;
	}
	public void setJsonDelete(String jsonDelete) {
		this.jsonDelete = jsonDelete;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	
}
