package com.xbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ASSIGNMENT")
public class Assignment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="BIODATA_ID")
	private Biodata biodataAssg;
	
	private String title;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="START_DATE")
	private Date startDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_DATE")
	private Date endDate;
	private String description;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REALIZATION_DATE")
	private Date realizationDate;
	private String notes;
	@Column(name="IS_HOLD")
	private int isHoled;
	@Column(name="IS_DONE")
	private int isDone;
	@Column(name="CREATED_BY")
	private Long createdBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON")
	private Date createdOn;
	@Column(name="MODIFIED_BY")
	private Long modifiedBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFIED_ON")
	private Date modfiedOn;
	@Column(name="DELETE_BY")
	private Long deleteBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DELETE_ON")
	private Date deleteOn;
	@Column(name="IS_DELETE")
	private int isDelete;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Biodata getBiodataAssg() {
		return biodataAssg;
	}
	public void setBiodataAssg(Biodata biodataAssg) {
		this.biodataAssg = biodataAssg;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getRealizationDate() {
		return realizationDate;
	}
	public void setRealizationDate(Date realizationDate) {
		this.realizationDate = realizationDate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getIsHoled() {
		return isHoled;
	}
	public void setIsHoled(int isHoled) {
		this.isHoled = isHoled;
	}
	public int getIsDone() {
		return isDone;
	}
	public void setIsDone(int isDone) {
		this.isDone = isDone;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModfiedOn() {
		return modfiedOn;
	}
	public void setModfiedOn(Date modfiedOn) {
		this.modfiedOn = modfiedOn;
	}
	public Long getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}
	public Date getDeleteOn() {
		return deleteOn;
	}
	public void setDeleteOn(Date deleteOn) {
		this.deleteOn = deleteOn;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
