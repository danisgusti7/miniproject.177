package com.xbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_MONITORING")
public class Monitoring {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="BIODATA_ID")
	private Biodata biodataMon;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="IDLE_DATE")
	private Date idleDate;
	@Column(name="LAST_PROJECT")
	private String lastProject;
	@Column(name="IDLE_REASON")
	private String idleReason;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PLACEMENT_DATE")
	private Date placementDate;
	@Column(name="PLACEMENT_AT")
	private String placementAt;
	private String notes;
	private Long createdBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON")
	private Date createdOn;
	@Column(name="MODIVIED_BY")
	private Long modifiedBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFIED_ON")
	private Date modifiedOn;
	@Column(name="DELETE_BY")
	private Long deleteBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DELETE_ON")
	private Date deleteOn;
	@Column(name="IS_DELETE")
	private int isDelete;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Biodata getBiodataMon() {
		return biodataMon;
	}
	public void setBiodataMon(Biodata biodataMon) {
		this.biodataMon = biodataMon;
	}
	public Date getIdleDate() {
		return idleDate;
	}
	public void setIdleDate(Date idleDate) {
		this.idleDate = idleDate;
	}
	public String getLastProject() {
		return lastProject;
	}
	public void setLastProject(String lastProject) {
		this.lastProject = lastProject;
	}
	public String getIdleReason() {
		return idleReason;
	}
	public void setIdleReason(String idleReason) {
		this.idleReason = idleReason;
	}
	public Date getPlacementDate() {
		return placementDate;
	}
	public void setPlacementDate(Date placementDate) {
		this.placementDate = placementDate;
	}
	public String getPlacementAt() {
		return placementAt;
	}
	public void setPlacementAt(String placementAt) {
		this.placementAt = placementAt;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Long getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}
	public Date getDeleteOn() {
		return deleteOn;
	}
	public void setDeleteOn(Date deleteOn) {
		this.deleteOn = deleteOn;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
