package com.xbc.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="T_BIODATA")
public class Biodata {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String gender;
	@Column(name="LAST_EDUCATION")
	private String lastEducation;
	@Column(name="GRADUATION_YEAR")
	private String graduationYear;
	@Column(name="EDUCATIONAL_LEVEL")
	private String educationalLevel;
	private String majors;
	private String gpa;
	@ManyToOne
	@JoinColumn(name="BOOTCAMP_TEST_TYPE_ID")
	private BootcampTestType bootcampTestTypeId;
	private Integer iq;
	private String du;
	private Integer arithmetic;
	@Column(name="NESTED_LOGIC")
	private Integer nestedLogic;
	@Column(name="JOIN_TABLE")
	private Integer joinTable;
	private String tro;
	private String notes;
	private String interviewer;
	@Column(name="CREATED_BY")
	private Long createdBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON")
	private Date createdOn;
	@Column(name="MODIFIED_BY")
	private Long modifiedBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFIED_ON")
	private Date modifiedOn;
	private int active;
	@JsonIgnore
	@OneToMany(mappedBy="biodataMon")//nama object yang di monitoring/ yg di many to one
	private List<Monitoring> monitorings;
	@JsonIgnore
	@OneToMany(mappedBy="biodataAssg")//nama object yang di Assignment/ yg di many to one
	private List<Assignment> assignments;
	
	
	public List<Monitoring> getMonitorings() {
		return monitorings;
	}
	public void setMonitorings(List<Monitoring> monitorings) {
		this.monitorings = monitorings;
	}
	public List<Assignment> getAssignments() {
		return assignments;
	}
	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getEducationalLevel() {
		return educationalLevel;
	}
	public void setEducationalLevel(String educationalLevel) {
		this.educationalLevel = educationalLevel;
	}
	public String getMajors() {
		return majors;
	}
	public void setMajors(String majors) {
		this.majors = majors;
	}
	public String getGpa() {
		return gpa;
	}
	public void setGpa(String gpa) {
		this.gpa = gpa;
	}
	
	public BootcampTestType getBootcampTestTypeId() {
		return bootcampTestTypeId;
	}
	public void setBootcampTestTypeId(BootcampTestType bootcampTestTypeId) {
		this.bootcampTestTypeId = bootcampTestTypeId;
	}
	public Integer getIq() {
		return iq;
	}
	public void setIq(Integer iq) {
		this.iq = iq;
	}
	public String getDu() {
		return du;
	}
	public void setDu(String du) {
		this.du = du;
	}
	public Integer getArithmetic() {
		return arithmetic;
	}
	public void setArithmetic(Integer arithmetic) {
		this.arithmetic = arithmetic;
	}
	public Integer getNestedLogic() {
		return nestedLogic;
	}
	public void setNestedLogic(Integer nestedLogic) {
		this.nestedLogic = nestedLogic;
	}
	public Integer getJoinTable() {
		return joinTable;
	}
	public void setJoinTable(Integer joinTable) {
		this.joinTable = joinTable;
	}
	public String getTro() {
		return tro;
	}
	public void setTro(String tro) {
		this.tro = tro;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getInterviewer() {
		return interviewer;
	}
	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
		
}
