package com.xbc.model;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name="T_MENU_ACCESS")

public class MenuAccess {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name="MENU_ID")
	private Menu menuMenuAccess;
	
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private Role roleMenuAccess;
	
	@Column(name="CREATED_BY")
	private Long createdBy;
	@Column(name="CREATED_ON")
	private Date createdOn;
	

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	
	public Menu getMenuMenuAccess() {
		return menuMenuAccess;
	}
	public void setMenuMenuAccess(Menu menuMenuAccess) {
		this.menuMenuAccess = menuMenuAccess;
	}
	public Role getRoleMenuAccess() {
		return roleMenuAccess;
	}
	public void setRoleMenuAccess(Role roleMenuAccess) {
		this.roleMenuAccess = roleMenuAccess;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	
}
